# Semantic BMS
Welcome to the Semantic BMS framework project site.

The SemanticBMS project aims to provide semantic information for building 
automation data. More details about the topic can be found in the doctoral thesis of the author:
https://is.muni.cz/th/umgby

The Source files in this repository are proof-of-concept implementations of following
components of the middleware layer:
 * **Semantic API** - SemanticAPI & Ontology repository, currently in relatively stable version 1.0
 * **Data Access API** - Data Access API, in a phase of development snapshots
 * **Use-case Plugins** - Data providers for the BACnet automation protocol and Delta Historian archive server
 * **SemanticBMSClient** - Client for querying the Semantic API
 * **TestBench** - Tools for generating the sample data set and testing the query performance + pre-generated sample data

The project is developed and by maintained by Lasaris lab at Faculty of 
Informatics, Masaryk University, Brno, Czech Republic.

The main developer is Adam Kučera. Contact: **akucera@mail.muni.cz**

ResearchGate profile: https://www.researchgate.net/profile/Adam_Kucera/

## Sample Semantic BMS Ontology data
The sample data can be found in the [TestBench/NT-Data/sample.nt](https://gitlab.fi.muni.cz/xkucer16/semanticBMS/raw/master/TestBench/NT-Data/sample.nt) file.

For more information about the data set, see [TestBench readme](https://gitlab.fi.muni.cz/xkucer16/semanticBMS/tree/master/TestBench/README.md).

## How To Install SemanticBMS

This section describes one of the ways to deploy the Semantic BMS APIs. There are certainly many other ways, however this one
is tested and pre-configured.

To deploy the application in Apache Tomcat container (tested for Tomcat 8.5 and higher, requires Servlet 3.0 implementation):
* Download and install the Apache Tomcat (for Windows, ZIP distribution is sufficient)
* Preferred and pre-configured location for Tomcat installation is C:\apache-tomcat
* Create a directory named sbms in the webapps folder of your tomcat installation (e.g. C:\apache-tomcat\webapps\sbms)
* Copy the files from the SemanticBMSClient project into the sbms/client directory
* If your Tomcat installation directory is not C:\apache-tomcat:
  * Set up the SemanticAPI to use the desired TDB location by updating the tdb.path property in the semantics.properties file 
   (The TDB creation itself is described in the following steps) 
  * Check the ont-policy.rdf file and update paths to the local ontology definitions
* Build the SemanticAPI/DataAccessAPI project using maven:
```
    mvn clean install
```
* Copy the resulting build (located in <Your git repository>/semanticBMS/SemanticAPI/target/SemanticAPI-1.0 for the SemanticAPI) 
  into the new directory directory (e.g. C:\apache-tomcat\webapps\sbms) - the WEB-INF and META-INF folders 
  are meant to be placed directly in the sbms directory (and merged from both projects -- SemanticAPI and DataAccesAPI)
* Prepare the TDB and place it under the webapps/sbms/WEB-INF/tdb directory. There are multiple ways to prepare the TDB. Refer to the TestBench/README.md file.
* Run the Apache Tomcat
* The APIs are available at http://localhost:8080/sbms/semantics and http://localhost:8080/sbms/data
* To test the Semantic API, visit http://localhost:8080/sbms/semantics/types/type or run the performance tests.
* The simple client for testing purposes is available at http://localhost:8080/sbms/client/query.html (default credentials are user/resu and admin/nimda)


