package cz.muni.fi.lasaris.sbms.semantics.api.entities;

import java.util.SortedSet;
import java.util.TreeSet;

public class Algorithm extends Address {

	private SortedSet<Address> usedAddressList; 
	
	public SortedSet<Address> getUsedAddressList() {
		return usedAddressList;
	}

	public void setUsedAddressList(SortedSet<Address> usedDataPointList) {
		this.usedAddressList = usedDataPointList;
	}
	
	public Address getUsedAddress() {
		return usedAddressList.first();
	}
	
	public void setUsedAddress(Address a) {
		usedAddressList = new TreeSet<Address>();
		usedAddressList.add(a);
	}

	public Algorithm() {
	}

	public Algorithm(String bmsId) {
		super(bmsId);
	}

	public Algorithm(String bmsId, String type) {
		super(bmsId, type);
		
	}

}
