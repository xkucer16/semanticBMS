package cz.muni.fi.lasaris.sbms.semantics.api.request;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public abstract class Request {
	protected List<String> responseFields;
	
	public Request() {
		responseFields = new ArrayList<String>();
	}

	public List<String> getResponseFields() {
		return responseFields;
	}
	
	public void parseResponseFields(String fields) {
		if(fields != null) {
			for(String f : fields.split(",")) {
				if(StringUtils.isNotBlank(f)) {
					responseFields.add(f);
				}
			}
		}
	}
	
	public abstract String getIdentityField();
}
