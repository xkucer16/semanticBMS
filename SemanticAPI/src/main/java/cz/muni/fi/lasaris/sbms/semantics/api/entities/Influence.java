package cz.muni.fi.lasaris.sbms.semantics.api.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Influence implements Groupable {
	private ObservedProperty property;
	private Scope scope;
	
	public Influence(){
	}

	public Influence(ObservedProperty property, Scope scope) {
		this.property = property;
		this.scope = scope;
	}

	public ObservedProperty getProperty() {
		return property;
	}

	public void setProperty(ObservedProperty property) {
		this.property = property;
	}

	public Scope getScope() {
		return scope;
	}

	public void setScope(Scope scope) {
		this.scope = scope;
	}

	@Override
	public String toString() {
		return "Influence [property=" + property + ", scope=" + scope + "]";
	}

	private String group;
	
	@Override
	public String getGroup() {
		return group;
	}

	@Override
	public void setGroup(String group) {
		this.group = group;
		
	}	
}
