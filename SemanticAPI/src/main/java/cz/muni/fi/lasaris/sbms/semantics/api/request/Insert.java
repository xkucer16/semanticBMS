package cz.muni.fi.lasaris.sbms.semantics.api.request;

import javax.xml.bind.annotation.XmlRootElement;

import cz.muni.fi.lasaris.sbms.semantics.api.entities.Address;
import cz.muni.fi.lasaris.sbms.semantics.api.entities.Algorithm;
import cz.muni.fi.lasaris.sbms.semantics.api.entities.DataPoint;
import cz.muni.fi.lasaris.sbms.semantics.api.entities.Trend;

@XmlRootElement
public class Insert {
	
	private DataPoint dataPoint;
	private Trend trend;
	private Algorithm algorithm;
	private Address address;
	
	
	public DataPoint getDataPoint() {
		return dataPoint;
	}

	public void setDataPoint(DataPoint dataPoint) {
		this.dataPoint = dataPoint;
	}

	public Trend getTrend() {
		return trend;
	}

	public void setTrend(Trend trend) {
		this.trend = trend;
	}

	public Algorithm getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(Algorithm algorithm) {
		this.algorithm = algorithm;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Insert [dataPoint=" + dataPoint + ", trend=" + trend + ", algorithm=" + algorithm + ", address="
				+ address + "]";
	}

	
	
	/*
	@Override
	public String toString() {
		return "DataPointInsert [bmsId=" + dataPoint.getBmsId() + ", dataPointClass=" + dataPoint.getDataPointType() 
		+ ", sensingClass=" + dataPoint.getSensing().getType() + ", quality=" + dataPoint.getProperty().getQuality() 
		+ ", propDomain=" + dataPoint.getProperty().getDomain() + ", timeWindow=" + dataPoint.getSensing().getWindow()
				+ ", scope=" + dataPoint.getScope().getBimId() + ", source=" + dataPoint.getSource().getBimId() + 
				", influence=[" + dataPoint.getInfluence().getScope().getBimId() + "," 
				+ dataPoint.getInfluence().getProperty().getDomain() + ", " 
				+ dataPoint.getInfluence().getProperty().getQuality() + "]";
	}
	*/
	/*
	private String bmsId;
	private String dataPointClass;
	private String sensingClass;
	private String quality;
	private String propDomain;
	private String timeWindow;
	private String scope;
	private String source;
	public String getBmsId() {
		return bmsId;
	}
	public void setBmsId(String bmsId) {
		this.bmsId = bmsId;
	}
	public String getDataPointClass() {
		return dataPointClass;
	}
	public void setDataPointClass(String dataPointClass) {
		this.dataPointClass = dataPointClass;
	}
	public String getSensingClass() {
		return sensingClass;
	}
	public void setSensingClass(String sensingClass) {
		this.sensingClass = sensingClass;
	}
	public String getQuality() {
		return quality;
	}
	public void setQuality(String quality) {
		this.quality = quality;
	}
	public String getPropDomain() {
		return propDomain;
	}
	public void setPropDomain(String propDomain) {
		this.propDomain = propDomain;
	}
	public String getTimeWindow() {
		return timeWindow;
	}
	public void setTimeWindow(String timeWindow) {
		this.timeWindow = timeWindow;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	@Override
	public String toString() {
		return "DataPointInsert [bmsId=" + bmsId + ", dataPointClass=" + dataPointClass + ", sensingClass="
				+ sensingClass + ", quality=" + quality + ", propDomain=" + propDomain + ", timeWindow=" + timeWindow
				+ ", scope=" + scope + ", source=" + source + "]";
	}
	*/
}
