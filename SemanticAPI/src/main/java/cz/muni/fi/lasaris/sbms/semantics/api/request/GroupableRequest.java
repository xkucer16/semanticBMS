package cz.muni.fi.lasaris.sbms.semantics.api.request;

import cz.muni.fi.lasaris.sbms.semantics.logic.Fields;

public abstract class GroupableRequest extends Request {

	
	private String grouping;
	private String[] g;

	public String getGrouping() {
		return grouping;
	}

	public String getGrouping(int i) {
		if(g == null || i < 0 || i > 2) {
			return Fields.DEFAULT_GROUPING;
		}
		return g[i];
	}

	public void setGrouping(String grouping) {
		this.grouping = grouping;
		if(grouping != null) {
			String[] gs = grouping.split("\\.");
			this.g =(gs.length > 1) ? new String[] {gs[0], Character.toUpperCase(gs[1].charAt(0)) + gs[1].substring(1)}
			: new String[] {gs[0], gs[0]};
		} else {
			this.g = new String[] { Fields.DEFAULT_GROUPING, Fields.DEFAULT_GROUPING };
		}
	}

}