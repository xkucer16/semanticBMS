package cz.muni.fi.lasaris.sbms.semantics.api.entities;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Address {

	protected String bmsId;
	protected String type;
	protected Source publisher;
	
	public String getBmsId() {
		return bmsId;
	}
	public void setBmsId(String bmsId) {
		this.bmsId = bmsId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@JsonIgnore
	public String getResourceId() {
		return Address.getResourceId(this.bmsId);
	}
	
	public static String getResourceId(String bmsId) {
		return StringEscapeUtils.escapeXml10(bmsId);
	}
	
	private List<Influence> influence;
	
	public List<Influence> getInfluenceList() {
		return influence;
	}
	
	public Influence getInfluenced() {
		return (influence == null || influence.size() == 0) ? null : influence.get(0);
	}
	
	public void setInfluenceList(List<Influence> influence) {
		this.influence = influence;
	}
	
	public void setInfluenced(Influence i) {
		this.influence = new LinkedList<Influence>();
		this.influence.add(i);
	}
	
	public Source getPublisher() {
		return publisher;
	}
	public void setPublisher(Source publisher) {
		this.publisher = publisher;
	}
	public Address() {
	}
	
	public Address(String bmsId) {
		this.bmsId = bmsId;
	}
	
	public Address(String bmsId, String type) {
		this.bmsId = bmsId;
		this.type = type;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bmsId == null) ? 0 : bmsId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		if (bmsId == null) {
			if (other.bmsId != null)
				return false;
		} else if (!bmsId.equals(other.bmsId))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Address [bmsId=" + bmsId + ", type=" + type + "]";
	}
	
}
