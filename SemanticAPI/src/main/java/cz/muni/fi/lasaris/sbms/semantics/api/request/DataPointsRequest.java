package cz.muni.fi.lasaris.sbms.semantics.api.request;

import javax.xml.bind.annotation.XmlRootElement;

import cz.muni.fi.lasaris.sbms.semantics.api.entities.DataPoint;

@XmlRootElement
public class DataPointsRequest extends GroupableRequest {
	
	
	private DataPoint dataPoint;
	public DataPoint getDataPoint() {
		return dataPoint;
	}
	
	public void setDataPoint(DataPoint dp) {
		this.dataPoint = dp;
	}

	@Override
	public String getIdentityField() {
		return "bmsId";
	}
	
}
