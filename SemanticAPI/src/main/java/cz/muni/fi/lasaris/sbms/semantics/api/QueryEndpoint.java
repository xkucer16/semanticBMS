package cz.muni.fi.lasaris.sbms.semantics.api;

import java.io.IOException;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import cz.muni.fi.lasaris.sbms.semantics.logic.TdbConnector;
@PermitAll
@Path("query")
public class QueryEndpoint {
	
	final static Logger logger = Logger.getLogger(QueryEndpoint.class);
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@RolesAllowed("admin")
	public String GetQueryResult(@QueryParam("sparql") String sparql, @QueryParam("format") String format) throws IOException {
		return TdbConnector.executeSPARQLFormatted(sparql, format);
	}
}
