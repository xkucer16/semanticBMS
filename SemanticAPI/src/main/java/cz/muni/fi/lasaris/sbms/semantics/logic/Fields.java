package cz.muni.fi.lasaris.sbms.semantics.logic;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.List;


public class Fields {

	public static final List<String> GROUPINGS = Collections.unmodifiableList(Arrays.asList(new String[]{
	Fields.DEFAULT_GROUPING,
	"scope.type",
	"scope.room",
	"scope.floor",
	"scope.building",
	"scope.site",
	"source.type",
	"source.room",
	"source.floor",
	"source.building",
	"source.site",
	"dataPoint.type"
	}));
	
	public static final String DEFAULT_GROUPING = "noGrouping";
	
	public static final List<String> DATAPOINT_FILTER_FIELDS = Collections.unmodifiableList(Arrays.asList(new String[]{
			"bmsId",
			"type",
			"source.bimId",
			"source.type",
			"source.location",
			"scope.bimId",
			"scope.type",
			"scope.location",
			"sensing.type",
			"sensing.window",
			"property.domain",
			"property.quality",
			"publisher.bimId",
			"influenced.scope.bimId",
			"influenced.property.quality",
			"influenced.property.domain"
	}));
	public static final List<String> DATAPOINT_DATA_FIELDS = Collections.unmodifiableList(Arrays.asList(new String[]{
			"bmsId",
			"type",
			"source.bimId",
			"source.type",
			"source.location",
			"scope.bimId",
			"scope.type",
			"scope.location",
			"sensing.type",
			"sensing.window",
			"property.domain",
			"property.quality",
			"publisher.bimId"
	}));
	
	
	public static final List<String> TREND_FILTER_FIELDS = Collections.unmodifiableList(Arrays.asList(new String[]{
			"bmsId",
			"dataPoint.bmsId",
			"dataPoint.type",
			"dataPoint.source.bimId",
			"dataPoint.source.type",
			"dataPoint.source.location",
			"dataPoint.scope.bimId",
			"dataPoint.scope.type",
			"dataPoint.scope.location",
			"dataPoint.sensing.type",
			"dataPoint.sensing.window",
			"dataPoint.property.domain",
			"dataPoint.property.quality",
			"dataPoint.publisher.bimId",
			"dataPoint.influenced.scope.bimId",
			"dataPoint.influenced.property.quality",
			"dataPoint.influenced.property.domain"
	}));
	public static final List<String> TREND_DATA_FIELDS = Collections.unmodifiableList(Arrays.asList(new String[]{
			"bmsId",
			"dataPoint.bmsId",
			"dataPoint.type",
			"dataPoint.source.bimId",
			"dataPoint.source.type",
			"dataPoint.source.location",
			"dataPoint.scope.bimId",
			"dataPoint.scope.type",
			"dataPoint.scope.location",
			"dataPoint.sensing.type",
			"dataPoint.sensing.window",
			"dataPoint.property.domain",
			"dataPoint.property.quality",
			"dataPoint.publisher.bimId"
	}));
	
	public static final List<String> INFLUENCE_DATA_FIELDS = Collections.unmodifiableList(Arrays.asList(new String[]{
			"bmsId",
			"type",
			"publisher.bimId",
			"influenced.scope.bimId",
			"influenced.scope.type",
			"influenced.property.quality",
			"influenced.property.domain"
	}));
	
	public static final List<String> INFLUENCED_FILTER_FIELDS = Collections.unmodifiableList(Arrays.asList(new String[]{
			"bmsId"
	}));
	
	public static final List<String> INFLUENCING_FILTER_FIELDS = Collections.unmodifiableList(Arrays.asList(new String[]{
			"influenced.scope.bimId",
			"influenced.property.quality",
			"influenced.property.domain"
	}));
	
	public static final List<String> USED_FIELDS = Collections.unmodifiableList(Arrays.asList(new String[]{
			"bmsId",
			"used.bmsId",
			"used.type"
	}));
	
	public static final List<String> INSERT_FIELDS_DP = Collections.unmodifiableList(Arrays.asList(new String[]{
			"bmsId",
			"type",
			"source.bimId",
			"scope.bimId",
			"sensing.type",
			"sensing.window",
			"property.domain",
			"property.quality",
			"publisher.bimId"
	}));
	
	public static final List<String> INSERT_FIELDS_TREND = Collections.unmodifiableList(Arrays.asList(new String[]{
			"bmsId",
			"dataPoint.bmsId"
	}));
	
	public static final List<String> INSERT_FIELDS_INFLUENCE = Collections.unmodifiableList(Arrays.asList(new String[]{
			"bmsId",
			"type",
			"influenced.scope.bimId",
			"influenced.property.quality",
			"influenced.property.domain"
	}));
	
	public static final List<String> INSERT_FIELDS_USAGE = Collections.unmodifiableList(Arrays.asList(new String[]{
			"bmsId",
			"used.bmsId",
			"used.type"
	}));
	
	public static final Map<String, String> FIELD_SOURCES = createMap(new String[] {
			"bmsId", "bms",
			"type", "sbms-tree",
			"source.bimId", "bim",
			"source.type", "sbms-tree",
			"source.location", "bim",
			"scope.bimId", "bim",
			"scope.type", "sbms",
			"scope.location", "bim",
			"sensing.type", "sbms-tree",
			"sensing.window", "sbms-enum",
			"property.domain", "sbms-enum",
			"property.quality", "sbms-enum",
			"publisher.bimId", "bim",
			"influenced.scope.bimId", "bim",
			"influenced.scope.type", "sbms",
			"influenced.property.quality", "sbms-enum",
			"influenced.property.domain", "sbms-enum",
			"dataPoint.bmsId", "bms",
			"dataPoint.type", "sbms-tree",
			"dataPoint.source.bimId", "bim",
			"dataPoint.source.type", "sbms-tree",
			"dataPoint.source.location", "bim",
			"dataPoint.scope.bimId", "bim",
			"dataPoint.scope.type", "sbms",
			"dataPoint.scope.location", "bim",
			"dataPoint.sensing.type", "sbms-tree",
			"dataPoint.sensing.window", "sbms-enum",
			"dataPoint.property.domain", "sbms-enum",
			"dataPoint.property.quality", "sbms-enum",
			"dataPoint.publisher.bimId", "bim",
			"dataPoint.influenced.scope.bimId", "bim",
			"dataPoint.influenced.scope.type", "sbms",
			"dataPoint.influenced.property.quality", "sbms-enum",
			"dataPoint.influenced.property.domain", "sbms-enum"
	});
	
	private static Map<String, String> createMap(String[] array) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		
		for(int i = 0; i < array.length / 2; i++) {
			map.put(array[i*2], array[i*2+1]);
		}
		
		return map;
	}
}
