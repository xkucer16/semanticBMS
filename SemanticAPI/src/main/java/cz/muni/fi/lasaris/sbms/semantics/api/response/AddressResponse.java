package cz.muni.fi.lasaris.sbms.semantics.api.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

import cz.muni.fi.lasaris.sbms.semantics.api.entities.Address;

@XmlRootElement
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AddressResponse {
	private String error;
	
	private List<Address> results;

	public List<Address> getResults() {
		return results;
	}
	
	public void addResult(Address a) {
		if(results == null) {
			results = new ArrayList<Address>();
		}
		results.add(a);
	}
	
	public void setResults(List<Address> results) {
		this.results = results;
	}

	public static AddressResponse getErrorResponse(String error) {
		AddressResponse r = new AddressResponse();
		r.setError(error);
		return r;
	}
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

}
