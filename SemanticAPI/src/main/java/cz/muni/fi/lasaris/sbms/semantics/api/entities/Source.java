package cz.muni.fi.lasaris.sbms.semantics.api.entities;

import org.apache.commons.lang3.StringEscapeUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Source {
	private String bimId;
	private String type;
	private String location;
	
	public Source() {
	}

	public Source(String bimId, String type, String locationBimId) {
		this.bimId = bimId;
		this.type = type;
		this.location = locationBimId;
	}

	public String getBimId() {
		return bimId;
	}

	public void setBimId(String bimId) {
		this.bimId = bimId;
	}
	
	public static String getResourceId(String bimId) {
		return StringEscapeUtils.escapeXml10(bimId);
	}
	
	@JsonIgnore
	public String getResourceId() {
		return Source.getResourceId(this.bimId);
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String locationBimId) {
		this.location = locationBimId;
	}

	@Override
	public String toString() {
		return "Source [bimId=" + bimId + ", type=" + type + ", locationBimId=" + location + "]";
	}
	
	
}
