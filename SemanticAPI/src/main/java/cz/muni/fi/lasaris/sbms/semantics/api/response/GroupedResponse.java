package cz.muni.fi.lasaris.sbms.semantics.api.response;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

import cz.muni.fi.lasaris.sbms.semantics.api.entities.Groupable;

@XmlRootElement
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GroupedResponse {
	
	private Map<String, List<Groupable>> results;
	
	
	public Map<String,List<Groupable>> getResults() {
		return results;
	}

	
	public Set<String> getGroups() {
		return new TreeSet<String>(results.keySet());
	}
	
	
	public GroupedResponse() {
		results = new LinkedHashMap<String,List<Groupable>>();
	}
	
	public void addResult(Groupable result) {
		if(!results.containsKey(result.getGroup())) {
			results.put(result.getGroup(), new LinkedList<Groupable>());
		}
		results.get(result.getGroup()).add(result);
	}
	
	private String error;
	
	public static GroupedResponse getErrorResponse(String error) {
		GroupedResponse r = new GroupedResponse();
		r.setError(error);
		return r;
	}
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

}
