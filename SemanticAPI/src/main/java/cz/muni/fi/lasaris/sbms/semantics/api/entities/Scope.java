package cz.muni.fi.lasaris.sbms.semantics.api.entities;

import org.apache.commons.lang3.StringEscapeUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Scope {
	private String bimId;
	private String type;
	//private String deviceType;
	private String location;
	
	public Scope() {
	}

	public Scope(String bimId, String type, String deviceLocationBimId) {
		this.bimId = bimId;
		this.type = type;
		//this.deviceType = deviceType;
		this.location = deviceLocationBimId;
	}

	public Scope(String bimId, String type) {
		this.bimId = bimId;
		this.type = type;
	}

	public String getBimId() {
		return bimId;
	}

	public void setBimId(String bimId) {
		this.bimId = bimId;
	}
	
	public static String getResourceId(String bimId) {
		return StringEscapeUtils.escapeXml10(bimId);
	}
	
	@JsonIgnore
	public String getResourceId() {
		return Scope.getResourceId(this.bimId);
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	/*
	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	*/
	public String getLocation() {
		return location;
	}

	public void setLocation(String deviceLocationBimId) {
		this.location = deviceLocationBimId;
	}

	@Override
	public String toString() {
		return "Scope [bimId=" + bimId + ", type=" + type + ", deviceLocationBimId=" + location + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bimId == null) ? 0 : bimId.hashCode());
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Scope other = (Scope) obj;
		if (bimId == null) {
			if (other.bimId != null)
				return false;
		} else if (!bimId.equals(other.bimId))
			return false;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
	
	
}
