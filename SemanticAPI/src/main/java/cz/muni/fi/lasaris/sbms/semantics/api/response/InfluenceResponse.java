package cz.muni.fi.lasaris.sbms.semantics.api.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

import cz.muni.fi.lasaris.sbms.semantics.api.entities.Influence;

@XmlRootElement
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InfluenceResponse {
private String error;
	
	private List<Influence> results;

	public List<Influence> getResults() {
		return results;
	}
	
	public void addResult(Influence i) {
		if(results == null) {
			results = new ArrayList<Influence>();
		}
		results.add(i);
	}
	
	public void setResults(List<Influence> results) {
		this.results = results;
	}

	public static InfluenceResponse getErrorResponse(String error) {
		InfluenceResponse r = new InfluenceResponse();
		r.setError(error);
		return r;
	}
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
