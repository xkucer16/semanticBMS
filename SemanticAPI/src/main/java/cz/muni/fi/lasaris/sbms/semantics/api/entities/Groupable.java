package cz.muni.fi.lasaris.sbms.semantics.api.entities;

public interface Groupable {
	public String getGroup();
	public void setGroup(String group);
}
