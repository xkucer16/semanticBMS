package cz.muni.fi.lasaris.sbms.semantics.api.request;

import cz.muni.fi.lasaris.sbms.semantics.api.entities.Trend;

public class TrendsRequest extends GroupableRequest {
	
	private Trend trend;
	
	
	public Trend getTrend() {
		return trend;
	}


	public void setTrend(Trend trend) {
		this.trend = trend;
	}

	@Override
	public String getIdentityField() {
		return "trend.bmsId";
	}
	
	@Override
	public void parseResponseFields(String fields) {
		super.parseResponseFields(fields);
		for(int i = 0; i <  this.responseFields.size(); i++) {
			String f = this.responseFields.get(i);
			if(f.startsWith("dataPoint.")) {
				responseFields.set(i, f.substring(10));
			} else {
				responseFields.set(i, "trend." + f);
			}
		}
	}

}
