package cz.muni.fi.lasaris.sbms.semantics.api;

import java.util.Map;

import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import cz.muni.fi.lasaris.sbms.semantics.api.response.TypeResponse;
import cz.muni.fi.lasaris.sbms.semantics.logic.Fields;

@PermitAll
@Path("fields")
public class FieldsEndpoint {
	@GET
	@Path("/grouping")
	@Produces(MediaType.APPLICATION_JSON)
	public TypeResponse getGroupingTypes() {
		TypeResponse r = new TypeResponse();
		r.setName("grouping");
		r.getMembers().addAll(Fields.GROUPINGS);
		return r;
	}
	
	@GET
	@Path("/filter.dataPoint")
	@Produces(MediaType.APPLICATION_JSON)
	public TypeResponse getDPFilterFieldTypes() {
		TypeResponse r = new TypeResponse();
		r.setName("filter.dataPoint");
		r.getMembers().addAll(Fields.DATAPOINT_FILTER_FIELDS);
		return r;
	}
	
	@GET
	@Path("/data.dataPoint")
	@Produces(MediaType.APPLICATION_JSON)
	public TypeResponse getDPDataFieldTypes() {
		TypeResponse r = new TypeResponse();
		r.setName("data.dataPoint");
		r.getMembers().addAll(Fields.DATAPOINT_DATA_FIELDS);
		return r;
	}
	
	@GET
	@Path("/filter.trend")
	@Produces(MediaType.APPLICATION_JSON)
	public TypeResponse getTrendFilterFieldTypes() {
		TypeResponse r = new TypeResponse();
		r.setName("filter.trend");
		r.getMembers().addAll(Fields.TREND_FILTER_FIELDS);
		return r;
	}
	
	@GET
	@Path("/data.trend")
	@Produces(MediaType.APPLICATION_JSON)
	public TypeResponse getTrendDataFieldTypes() {
		TypeResponse r = new TypeResponse();
		r.setName("data.trend");
		r.getMembers().addAll(Fields.TREND_DATA_FIELDS);
		return r;
	}
	
	@GET
	@Path("/filter.influenced")
	@Produces(MediaType.APPLICATION_JSON)
	public TypeResponse getInfluencedFilterFieldTypes() {
		TypeResponse r = new TypeResponse();
		r.setName("filter.trend");
		r.getMembers().addAll(Fields.INFLUENCED_FILTER_FIELDS);
		return r;
	}
	
	@GET
	@Path("/filter.influencing")
	@Produces(MediaType.APPLICATION_JSON)
	public TypeResponse getInfluencingFilterFieldTypes() {
		TypeResponse r = new TypeResponse();
		r.setName("filter.trend");
		r.getMembers().addAll(Fields.INFLUENCING_FILTER_FIELDS);
		return r;
	}
	
	@GET
	@Path("/data.influence")
	@Produces(MediaType.APPLICATION_JSON)
	public TypeResponse getInfluenceDataFieldTypes() {
		TypeResponse r = new TypeResponse();
		r.setName("data.influence");
		r.getMembers().addAll(Fields.INFLUENCE_DATA_FIELDS);
		return r;
	}
	
	@GET
	@Path("/insert.dataPoint")
	@Produces(MediaType.APPLICATION_JSON)
	public TypeResponse getInsertFieldTypesDP() {
		TypeResponse r = new TypeResponse();
		r.setName("insert.dataPoint");
		r.getMembers().addAll(Fields.INSERT_FIELDS_DP);
		return r;
	}
	
	@GET
	@Path("/insert.trend")
	@Produces(MediaType.APPLICATION_JSON)
	public TypeResponse getInsertFieldTypesTrend() {
		TypeResponse r = new TypeResponse();
		r.setName("insert.trend");
		r.getMembers().addAll(Fields.INSERT_FIELDS_TREND);
		return r;
	}
	
	@GET
	@Path("/insert.influence")
	@Produces(MediaType.APPLICATION_JSON)
	public TypeResponse getInsertFieldTypesInfluence() {
		TypeResponse r = new TypeResponse();
		r.setName("insert.influence");
		r.getMembers().addAll(Fields.INSERT_FIELDS_INFLUENCE);
		return r;
	}
	
	@GET
	@Path("/insert.usage")
	@Produces(MediaType.APPLICATION_JSON)
	public TypeResponse getInsertFieldTypesUsage() {
		TypeResponse r = new TypeResponse();
		r.setName("insert.usage");
		r.getMembers().addAll(Fields.INSERT_FIELDS_USAGE);
		return r;
	}
	
	@GET
	@Path("/source")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String,String> getFieldSources() {
		Map<String,String> r = Fields.FIELD_SOURCES;
		return r;
	}
}
