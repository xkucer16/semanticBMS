package cz.muni.fi.lasaris.sbms.semantics.api.entities;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@XmlRootElement
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DataPoint extends Address implements Groupable {
	
	private Source source;
	private Scope scope;
	private Sensing sensing;
	private ObservedProperty property;
	
	@JsonIgnore
	private String group;
	
	public DataPoint() {
	}
	
	
	public DataPoint(String bmsId) {
		//this.bmsId = bmsId;
		super(bmsId);
	}
	
	public DataPoint(String bmsId, String dataPointType) {
		//this.bmsId = bmsId;
		//this.type = dataPointType;
		super(bmsId, dataPointType);
	}
	
	public Source getSource() {
		return source;
	}
	
	public void setSource(Source source) {
		this.source = source;
	}

	public Scope getScope() {
		return scope;
	}

	public void setScope(Scope scope) {
		this.scope = scope;
	}

	public Sensing getSensing() {
		return sensing;
	}

	public void setSensing(Sensing sensing) {
		this.sensing = sensing;
	}

	public ObservedProperty getProperty() {
		return property;
	}

	public void setProperty(ObservedProperty property) {
		this.property = property;
	}
	

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bmsId == null) ? 0 : bmsId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataPoint other = (DataPoint) obj;
		if (bmsId == null) {
			if (other.bmsId != null)
				return false;
		} else if (!bmsId.equals(other.bmsId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DataPoint [bmsId=" + bmsId + ", dataPointType=" + type + ", source=" + source + ", scope="
				+ scope + ", sensing=" + sensing + ", property=" + property + "]";
	}
}
