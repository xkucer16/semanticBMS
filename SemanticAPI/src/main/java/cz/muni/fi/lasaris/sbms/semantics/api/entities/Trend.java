package cz.muni.fi.lasaris.sbms.semantics.api.entities;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@XmlRootElement
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Trend extends Address implements Groupable {
	
	private DataPoint dataPoint;
	
	@JsonIgnore
	@Override
	public String getGroup() {
		return (dataPoint == null) ? null : dataPoint.getGroup();
	}

	@Override
	public void setGroup(String group) {
		throw new UnsupportedOperationException();
	}

	public DataPoint getDataPoint() {
		return dataPoint;
	}

	public void setDataPoint(DataPoint dataPoint) {
		this.dataPoint = dataPoint;
	}

}
