package cz.muni.fi.lasaris.sbms.semantics.api.entities;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Sensing {
	private String type;
	private String window;
	
	public Sensing() {
	}

	public Sensing(String type, String window) {
		this.type = type;
		this.window = window;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getWindow() {
		return window;
	}

	public void setWindow(String window) {
		this.window = window;
	}

	@Override
	public String toString() {
		return "Sensing [type=" + type + ", window=" + window + "]";
	}
	
	
}
