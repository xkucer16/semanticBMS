package cz.muni.fi.lasaris.sbms.semantics.api.request;

import cz.muni.fi.lasaris.sbms.semantics.api.entities.Algorithm;

public class AlgorithmRequest extends Request {

	private Algorithm algorithm;

	public Algorithm getAlgorithm() {
		return algorithm;
	}

	public AlgorithmRequest(Algorithm algorithm) {
		super();
		this.algorithm = algorithm;
	}

	public void setAlgorithm(Algorithm algorithm) {
		this.algorithm = algorithm;
	}

	@Override
	public String getIdentityField() {
		return "bmsId";
	}

}
