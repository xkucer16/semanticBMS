package cz.muni.fi.lasaris.sbms.semantics.api.response;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TypeResponse {
	private String name;
	private List<String> members;
	
	public TypeResponse() {
		members = new ArrayList<String>();
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<String> getMembers() {
		return members;
	}
	
	public String toString() {
		return name + ": " + Arrays.toString(members.toArray()); 
	}
	
	public void setMembers(List<String> members) {
		this.members = members;
	}
}
