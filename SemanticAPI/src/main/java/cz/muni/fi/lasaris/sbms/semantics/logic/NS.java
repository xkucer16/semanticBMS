package cz.muni.fi.lasaris.sbms.semantics.logic;

public class NS {
	static final String sbmsd = "http://is.muni.cz/www/255658/sbms/v2_0/SemanticBMSData#";
	static final String sbms = "http://is.muni.cz/www/255658/sbms/v2_0/SemanticBMS#";
	static final String sbim = "http://is.muni.cz/www/255658/sbms/v2_0/SemanticBIM#";
	static final String rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	static final String rdfs = "http://www.w3.org/2000/01/rdf-schema#";
	static final String dul = "http://www.loa.istc.cnr.it/ontologies/DUL.owl#";
	static final String ucum = "http://purl.oclc.org/NET/muo/ucum/physical-quality/";
	static final String muo = "http://purl.oclc.org/NET/muo/muo#";
	static final String owl = "http://www.w3.org/2002/07/owl#";
}
