package cz.muni.fi.lasaris.sbms.semantics.api;

import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import cz.muni.fi.lasaris.sbms.semantics.api.response.TypeResponse;
import cz.muni.fi.lasaris.sbms.semantics.logic.TypesProvider;

@PermitAll
@Path("types")
public class TypesEndpoint {
	
	@GET
	@Path("/{typeName}")
	@Produces(MediaType.APPLICATION_JSON)
	public TypeResponse getFieldMembers(@PathParam("typeName") String typeName) {
		//return TypesProvider.getDataPointTypes();
		switch(typeName) {
		case "type":
		case "dataPoint.type":
			return TypesProvider.getDataPointTypes();
		case "address.type":
			return TypesProvider.getAddressTypes();
		case "sensing.type":
		case "dataPoint.sensing.type":
			return TypesProvider.getSensingTypes();
		case "sensing.window":
		case "dataPoint.sensing.window":
			return TypesProvider.getTimeWindowTypes();
		case "property.quality":
		case "influenced.property.quality":
		case "dataPoint.property.quality":
		case "dataPoint.influenced.property.quality":
			return TypesProvider.getQualityTypes();
		case "property.domain":
		case "influenced.property.domain":
		case "dataPoint.property.domain":
		case "dataPoint.influenced.property.domain":
			return TypesProvider.getPropDomainTypes();
		case "scope.type":
		case "influenced.scope.type":
		case "dataPoint.scope.type":
		case "dataPoint.influenced.scope.type":
			return TypesProvider.getScopeTypes();
		case "source.type":
		case "influenced.source.type":
		case "dataPoint.source.type":
		case "dataPoint.influenced.source.type":
			return TypesProvider.getSourceTypes();
		
		default: 
			TypeResponse t = new TypeResponse();
			t.setName("unknownType");
			return t;
		}
	}
	
	@GET
	@Path("/{typeName}/{root}")
	@Produces(MediaType.APPLICATION_JSON)
	public TypeResponse getFieldMembersFromParent(@PathParam("typeName") String typeName,
			@PathParam("root") String root) {
		switch(typeName) {
		case "type":
		case "dataPoint.type":
			return TypesProvider.getDataPointTypes(root);
		case "address.type":
			return TypesProvider.getAddressTypes(root);
		case "sensing.type":
		case "dataPoint.sensing.type":
			return TypesProvider.getSensingTypes(root);
		case "source.type":
		case "influenced.source.type":
		case "dataPoint.source.type":
		case "dataPoint.influenced.source.type":
			return TypesProvider.getSourceTypes(root);	
		default: 
			TypeResponse t = new TypeResponse();
			t.setName("unknownType");
			return t;
		}
	}
	
}
