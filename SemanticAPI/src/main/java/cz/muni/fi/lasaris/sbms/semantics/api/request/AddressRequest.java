package cz.muni.fi.lasaris.sbms.semantics.api.request;

import cz.muni.fi.lasaris.sbms.semantics.api.entities.Address;

public class AddressRequest extends Request {
	private Address address;
	
	public AddressRequest() {
		super();
	}
	
	public AddressRequest(Address address) {
		super();
		this.address = address;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public String getIdentityField() {
		return "bmsId";
	}
	
}
