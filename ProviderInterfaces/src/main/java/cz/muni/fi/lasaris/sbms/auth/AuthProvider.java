package cz.muni.fi.lasaris.sbms.auth;

import java.util.Properties;

public interface AuthProvider {

	void init(Properties props);
	
	boolean authenticate(String user, String password);

	boolean authorize(String user, String role);

}
