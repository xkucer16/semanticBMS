package cz.muni.fi.lasaris.sbms.data.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AddressGroup {
	private List<Address> addresses;
	private String name;
	public List<Address> getAdresses() {
		return Collections.unmodifiableList(this.addresses);
	}
	public String getName() {
		return name;
	}
	public AddressGroup(List<Address> addresses, String name) {
		this.addresses = new ArrayList<Address>(addresses.size());
		Collections.copy(this.addresses, addresses);
		this.name = name;
	}
	
	public void addAddress(Address address) {
		this.addresses.add(address);
	}
	
	
	
}
