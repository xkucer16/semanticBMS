package cz.muni.fi.lasaris.sbms.data.entities.containers;

import java.time.ZonedDateTime;
import java.util.NavigableMap;
import java.util.TreeMap;


public class Series<V> extends CompositeDataContainer<ZonedDateTime, V> {
	
	public Series() {
		super(new TreeMap<ZonedDateTime, V>());
	}
	
	public Series(NavigableMap<ZonedDateTime, V> data) {
		super(data);
	}
	
	public NavigableMap<ZonedDateTime, V> getNavigableData() {
		return (NavigableMap<ZonedDateTime, V>)this.container;
	}
}

