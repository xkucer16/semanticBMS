package cz.muni.fi.lasaris.sbms.data.entities;

public enum AggregationWindow {
	NONE,
	DURATION,
	CRON;
	
	public static AggregationWindow fromString(String s) {
		if(s.toUpperCase().trim().equals("DURATION")) {
			return AggregationWindow.DURATION;
		}
		if(s.toUpperCase().trim().equals("CRON")) {
			return AggregationWindow.CRON;
		}
		if(s.toUpperCase().trim().equals("NONE")) {
			return AggregationWindow.NONE;
		}
		
		throw new IllegalArgumentException("Unknown sampling type.");
	}
	
}
