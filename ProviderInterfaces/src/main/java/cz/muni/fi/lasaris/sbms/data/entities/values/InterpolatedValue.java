package cz.muni.fi.lasaris.sbms.data.entities.values;

import cz.muni.fi.lasaris.sbms.data.entities.AggregationFunction;
import cz.muni.fi.lasaris.sbms.data.entities.DataType;
import cz.muni.fi.lasaris.sbms.data.entities.Interpolation;

public class InterpolatedValue extends Value {

	public InterpolatedValue(Object value, DataType dataType, Interpolation interpolation) {
		super(value, dataType);
		this.aggregation = AggregationFunction.NONE;
		this.interpolation = interpolation;
	}
	
	public InterpolatedValue(double value, Interpolation interpolation) {
		super(value);
		this.aggregation = AggregationFunction.NONE;
		this.interpolation = interpolation;
	}
	
	public InterpolatedValue(long value, Interpolation interpolation) {
		super(value);
		this.aggregation = AggregationFunction.NONE;
		this.interpolation = interpolation;
	}
	
	public InterpolatedValue(boolean value, Interpolation interpolation) {
		super(value);
		this.aggregation = AggregationFunction.NONE;
		this.interpolation = interpolation;
	}
	
	public InterpolatedValue(String value, Interpolation interpolation) {
		super(value);
		this.aggregation = AggregationFunction.NONE;
		this.interpolation = interpolation;
	}
	
	private InterpolatedValue(boolean error, Interpolation interpolation, String msg) {
		super(true, msg);
		this.interpolation = interpolation;
		this.aggregation = AggregationFunction.NONE;
	}
	
	public static InterpolatedValue getInterpolationError(Interpolation interpolation) {
		return new InterpolatedValue(true, interpolation, null);
	}
	
	public static InterpolatedValue getInterpolationError(Interpolation interpolation, String msg) {
		return new InterpolatedValue(true, interpolation, msg);
	}
	
}
