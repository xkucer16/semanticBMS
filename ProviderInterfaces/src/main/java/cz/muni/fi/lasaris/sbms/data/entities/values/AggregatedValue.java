package cz.muni.fi.lasaris.sbms.data.entities.values;

import cz.muni.fi.lasaris.sbms.data.entities.AggregationFunction;
import cz.muni.fi.lasaris.sbms.data.entities.DataType;
import cz.muni.fi.lasaris.sbms.data.entities.Interpolation;

public class AggregatedValue extends Value {

	public AggregatedValue(Object value, DataType dataType, AggregationFunction aggregation) {
		super(value, dataType);
		this.aggregation = aggregation;
		this.interpolation = Interpolation.NONE;
	}
	
	public AggregatedValue(double value, AggregationFunction aggregation) {
		super(value);
		this.aggregation = aggregation;
		this.interpolation = Interpolation.NONE;
	}
	
	public AggregatedValue(long value, AggregationFunction aggregation) {
		super(value);
		this.aggregation = aggregation;
		this.interpolation = Interpolation.NONE;
	}
	
	public AggregatedValue(boolean value, AggregationFunction aggregation) {
		super(value);
		this.aggregation = aggregation;
		this.interpolation = Interpolation.NONE;
	}
	
	private AggregatedValue(boolean error, AggregationFunction aggregation, String msg) {
		super(true, msg);
		this.interpolation = Interpolation.NONE;
		this.aggregation = aggregation;
	}
	
	public static AggregatedValue getAggregationError(AggregationFunction aggregation) {
		return new AggregatedValue(true, aggregation, null);
	}
	
	public static AggregatedValue getAggregationError(AggregationFunction aggregation, String msg) {
		return new AggregatedValue(true, aggregation, msg);
	}
}
