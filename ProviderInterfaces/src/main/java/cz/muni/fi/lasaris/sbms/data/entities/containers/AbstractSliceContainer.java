package cz.muni.fi.lasaris.sbms.data.entities.containers;

import java.util.Map;

import cz.muni.fi.lasaris.sbms.data.entities.Address;
import cz.muni.fi.lasaris.sbms.data.entities.values.Value;

public abstract class AbstractSliceContainer<V extends Value> extends CompositeDataContainer<Address, V> {

	public AbstractSliceContainer() {
		super();
	}
	
	public AbstractSliceContainer(Map<Address, V> data) {
		super(data);
	}
}
