package cz.muni.fi.lasaris.sbms.data.entities.containers;

import java.time.ZonedDateTime;
import java.util.NavigableMap;

import cz.muni.fi.lasaris.sbms.data.entities.values.Value;

public class Trend extends Series<Value> {
	
	public Trend() {
		super();
	}
	
	public Trend(NavigableMap<ZonedDateTime, Value> data) {
		super(data);
	}
}
