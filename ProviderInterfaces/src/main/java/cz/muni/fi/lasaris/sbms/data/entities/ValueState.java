package cz.muni.fi.lasaris.sbms.data.entities;

public enum ValueState {
	EMPTY,
	SUCCESS,
	FAIL
}
