package cz.muni.fi.lasaris.sbms.data.entities.containers;

import java.util.Map;

import cz.muni.fi.lasaris.sbms.data.entities.Address;
import cz.muni.fi.lasaris.sbms.data.entities.values.RawValue;

public class Snapshot extends AbstractSliceContainer<RawValue> {
	public Snapshot() {
		super();
	}
	
	public Snapshot(Map<Address, RawValue> data) {
		super(data);
	}
}
