package cz.muni.fi.lasaris.sbms.data.entities;

public class Address {
	
	private String id;
	private String name;
	private DataType dataType;
	private double[] weights;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getBmsId() {
		return id;
	}
	
	public void setBmsId(String id) {
		this.id = id;
	}
	
	public String getProtocol() {
		String[] r = id.split("://");
		if(r[0] == id) {
			return null;
		} else {
			return r[0];
		}
	}
	
	public String getId(boolean protocol) {
		if(protocol) {
			return getId();
		}
		String[] r = id.split("://");
		return r[r.length - 1];
	}
	/*
	public void setProtocol(String protocol) {
		if(protocol != null || getProtocol() == null && id != null) {
			this.id = protocol + "://" + id;
		}
	}
	*/
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double[] getWeights() {
		return weights;
	}
	public void setWeights(double[] weights) {
		this.weights = weights;
	}
	public DataType getDataType() {
		return dataType;
	}
	public void setDataType(DataType dataType) {
		this.dataType = dataType;
	}
	public Address(String id, String name, double[] weights) {
		this.id = id;
		this.name = name;
		this.weights = weights;
		this.dataType = DataType.REAL;
	}
	
	public Address(String id, double[] weights) {
		this.id = id;
		this.weights = weights;
		this.dataType = DataType.REAL;
	}
	
	public Address(String id) {
		this.id = id;
		this.dataType = DataType.UNKNOWN;
	}
	public Address(String id, String name) {
		this.id = id;
		this.name = name;
		this.dataType = DataType.UNKNOWN;
	}
	
	public Address(String id, String name, DataType dataType) {
		this.id = id;
		this.name = name;
		this.dataType = dataType;
	}
	
	public Address(String id, DataType dataType) {
		this.id = id;
		this.dataType = dataType;
	}
	
	public Address() {
		this.dataType = DataType.UNKNOWN;
	}
	
	public String toString(boolean fullDescription) {
		return (fullDescription ? "Address [id=" + id + ", name=" + name + ", dataType=" + dataType + "]" : this.toString());
	}
	
	@Override
	public String toString() {
		return id;
	}
}
