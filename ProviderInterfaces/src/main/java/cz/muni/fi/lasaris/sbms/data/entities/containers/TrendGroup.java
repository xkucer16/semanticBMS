package cz.muni.fi.lasaris.sbms.data.entities.containers;

import java.util.Map;

import cz.muni.fi.lasaris.sbms.data.entities.Address;

public class TrendGroup extends CompositeDataContainer<Address, Trend> {
	
	public TrendGroup() {
		super();
	}
	
	public TrendGroup(Map<Address, Trend> data) {
		super(data);
	}
	
}
