package cz.muni.fi.lasaris.sbms.data.entities.values;


import cz.muni.fi.lasaris.sbms.data.entities.AggregationFunction;
import cz.muni.fi.lasaris.sbms.data.entities.DataType;
import cz.muni.fi.lasaris.sbms.data.entities.Interpolation;
import cz.muni.fi.lasaris.sbms.data.entities.ValueState;

/**
 * @author akucera
 *
 */

public abstract class Value {
	private Object value;
	protected AggregationFunction aggregation;
	protected Interpolation interpolation;
	private DataType  dataType;
	private ValueState state;

	public Object getValue() {
		return value;
	}
	
	/*
	public int getValueAsInt() {
		return ((Integer)value).intValue();
	}
	
	public double getValueAsDouble() {
		return ((Double)value).doubleValue();
	}
	
	public boolean getValueAsBoolean() {
		return ((Boolean)value).booleanValue();
	}
	
	public String getValueAsString() {
		return value.toString();
	}
	*/
	
	public void setValue(Object value) {
		this.value = value;
	}
	
	public AggregationFunction getAggregation() {
		return aggregation;
	}
	
	public void setAggregation(AggregationFunction aggregation) {
		this.aggregation = aggregation;
	}

	public Interpolation getInterpolation() {
		return interpolation;
	}
	
	public void setInterpolation(Interpolation interpolation) {
		this.interpolation = interpolation;
	}
	
	public DataType getDataType() {
		return dataType;
	}
	
	public void setDataType(DataType dataType) {
		this.dataType = dataType;
	}
	
	public ValueState getState() {
		return state;
	}

	public void setState(ValueState state) {
		this.state = state;
	}
	
	/*
	public boolean isNull() {
		return value == null;
	}
	*/
	
	public Value(Object value, DataType dataType) {
		this.value = value;
		this.dataType = dataType;
		this.state = ValueState.SUCCESS;
	}
	
	protected Value(long value) {
		this.value = Long.valueOf(value);
		this.dataType = DataType.INTEGER;
		this.state = ValueState.SUCCESS;
	}
	
	protected Value(double value) {
		this.value = Double.valueOf(value);
		this.dataType = DataType.REAL;
		this.state = ValueState.SUCCESS;
	}
	
	protected Value(boolean value) {
		this.value = Boolean.valueOf(value);
		this.dataType = DataType.BOOL;
		this.state = ValueState.SUCCESS;
		
	}
	
	protected Value(String value) {
		this.value = value;
		this.dataType = DataType.TEXT;
		this.state = ValueState.SUCCESS;
	}
	
	/**
	 * Special constructor for value reading that ended with fault
	 */
	protected Value(boolean error, String msg) {
		this.value = msg;
		this.dataType = DataType.OTHER;
		this.state = ValueState.FAIL;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aggregation == null) ? 0 : aggregation.hashCode());
		result = prime * result + ((dataType == null) ? 0 : dataType.hashCode());
		result = prime * result + ((interpolation == null) ? 0 : interpolation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Value other = (Value) obj;
		if (aggregation != other.aggregation)
			return false;
		if (dataType != other.dataType)
			return false;
		if (interpolation != other.interpolation)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Value [value=" + value + ", state=" + state + ", dataType=" + dataType + ", aggregation=" + aggregation
				+ ", interpolation=" + interpolation + "]";
	}

	
	
	
}
