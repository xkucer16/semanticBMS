package cz.muni.fi.lasaris.sbms.data.entities;

import java.time.Duration;
import java.time.ZonedDateTime;

public class SeriesSpecs {
	private ZonedDateTime start;
	private ZonedDateTime end;
	private Sampling sampling;
	private Interpolation interpolation;
	private Duration duration;
	private String cron;
	private AggregationWindow window;
	private AggregationFunction windowAggregation;
	
	public ZonedDateTime getStart() {
		return start;
	}

	public ZonedDateTime getEnd() {
		return end;
	}

	public Sampling getSampling() {
		return sampling;
	}

	public Interpolation getInterpolation() {
		return interpolation;
	}

	public Duration getDuration() {
		return duration;
	}

	public String getCron() {
		return cron;
	}
	
	public AggregationFunction getWindowAggregation() {
		return this.windowAggregation;
	}
	
	public AggregationWindow getWindow() {
		return this.window;
	}
	
	public SeriesSpecs(ZonedDateTime start, ZonedDateTime end) {
		this.start = start;
		this.end = end;
		this.sampling = Sampling.NONE;
		this.window = AggregationWindow.NONE;
		this.windowAggregation = AggregationFunction.NONE;
	}

	public SeriesSpecs(ZonedDateTime start, ZonedDateTime end, Duration samplingDuration, Interpolation interpolation) {
		this.start = start;
		this.end = end;
		this.duration = samplingDuration;
		this.sampling = Sampling.DURATION;
		this.interpolation = interpolation;
		this.window = AggregationWindow.NONE;
		this.windowAggregation = AggregationFunction.NONE;
	}

	public SeriesSpecs(ZonedDateTime start, ZonedDateTime end, String samplingCron, Interpolation interpolation) {
		this.start = start;
		this.end = end;
		this.cron = samplingCron;
		this.sampling = Sampling.CRON;
		this.interpolation = interpolation;
		this.window = AggregationWindow.NONE;
		this.windowAggregation = AggregationFunction.NONE;
	}
	
	public SeriesSpecs(ZonedDateTime start, ZonedDateTime end, Duration windowDuration, AggregationFunction windowAggregation) {
		this.start = start;
		this.end = end;
		this.sampling = Sampling.NONE;
		this.window = AggregationWindow.DURATION;
		this.duration = windowDuration;
		this.windowAggregation = windowAggregation;
	}
	
	public SeriesSpecs(ZonedDateTime start, ZonedDateTime end, String windowCron, AggregationFunction windowAggregation) {
		this.start = start;
		this.end = end;
		this.sampling = Sampling.NONE;
		this.window = AggregationWindow.CRON;
		this.cron = windowCron;
		this.windowAggregation = windowAggregation;
	}
	
	public boolean isWithWindows() {
		return !(this.window.equals(AggregationWindow.NONE));
	}
	
	public boolean isWithSampling() {
		return !(this.sampling.equals(Sampling.NONE));
	}
}
