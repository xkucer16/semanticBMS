package cz.muni.fi.lasaris.sbms.data.providers;

import java.util.List;
import java.util.Map;
import java.util.Properties;

import cz.muni.fi.lasaris.sbms.data.entities.Address;
import cz.muni.fi.lasaris.sbms.data.entities.AggregationFunction;
import cz.muni.fi.lasaris.sbms.data.entities.containers.Snapshot;
import cz.muni.fi.lasaris.sbms.data.entities.values.AggregatedValue;
import cz.muni.fi.lasaris.sbms.data.entities.values.RawValue;

public interface DataPointsProvider {
	
	void init(Properties props, String name, String propertiesPrefix);
	void close();
	
	/**
	 * Returns single data point value
	 * @param dataPoint
	 * @param allowCache
	 * @return
	 */
	RawValue getDataPointValue(Address dataPoint, boolean allowCache);
	
	/**
	 * Returns values from multiple data points
	 * @param dataPoints
	 * @param allowCache
	 * @return
	 */
	Snapshot getDataPointValues(List<Address> dataPoints, boolean allowCache);
	
	/**
	 * Returns multiple data point values grouped as requested by the parameter
	 * @param dataPointGroups
	 * @param allowCache
	 * @return
	 */
	Map<String, Snapshot> getDataPointGroupedValues(Map<String, List<Address>> dataPointGroups, boolean allowCache);
	
	/**
	 *  computes aggregation over group of data points
	 * @param dataPoints
	 * @param aggregation
	 * @param allowCache
	 * @return
	 */
	AggregatedValue getDataPointsAggregation(List<Address> dataPoints, AggregationFunction aggregation, boolean allowCache);
	
	/**
	 *  computes aggregations for each group of data points (equivalent to repeated call to getDataPointsAggregation)
	 * @param dataPointGroups
	 * @param aggregation
	 * @param allowCache
	 * @return
	 */
	Map<String, AggregatedValue> getDataPointGroupAggregations(Map<String,List<Address>> dataPointGroups, AggregationFunction aggregation, boolean allowCache);
}
