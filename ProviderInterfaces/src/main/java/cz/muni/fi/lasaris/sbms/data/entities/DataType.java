package cz.muni.fi.lasaris.sbms.data.entities;

public enum DataType {
	BOOL,
	INTEGER,
	REAL,
	TEXT,
	OTHER,
	UNKNOWN
}
