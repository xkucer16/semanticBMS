package cz.muni.fi.lasaris.sbms.data.entities.containers;

import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;

import cz.muni.fi.lasaris.sbms.data.entities.Address;
import cz.muni.fi.lasaris.sbms.data.entities.Interpolation;
import cz.muni.fi.lasaris.sbms.data.entities.values.InterpolatedValue;
import cz.muni.fi.lasaris.sbms.data.entities.values.Value;
import cz.muni.fi.lasaris.sbms.data.util.Sampler;

public class Slice extends AbstractSliceContainer<InterpolatedValue> {
	
	public Slice() {
		super();
	}
	
	public Slice(Map<Address, InterpolatedValue> data) {
		super(data);
	}
	
	public Slice(Map<Address, Trend> trends, Interpolation i, ZonedDateTime targetTS) {
		super();
		for(Address a : trends.keySet()) {
			NavigableMap<ZonedDateTime, Value> t = trends.get(a).getNavigableData();
			if(t.size() > 2) {
				throw new IllegalArgumentException("trend is not suitable for a slice - it contains more than two records");
			}
			Entry<ZonedDateTime, Value> first = t.firstEntry();
			Entry<ZonedDateTime, Value> last = t.lastEntry();
			
			InterpolatedValue v = Sampler.getInterpolation(first.getKey(), first.getValue(), last.getKey(), last.getValue(), i, targetTS);
			this.container.put(a, v);
		}
	}
}
