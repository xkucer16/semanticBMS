package cz.muni.fi.lasaris.sbms.data.providers;

import java.util.List;
import java.util.Map;
import java.util.Properties;

import cz.muni.fi.lasaris.sbms.data.entities.Address;
import cz.muni.fi.lasaris.sbms.data.entities.AggregationFunction;
import cz.muni.fi.lasaris.sbms.data.entities.containers.Snapshot;
import cz.muni.fi.lasaris.sbms.data.entities.values.AggregatedValue;
import cz.muni.fi.lasaris.sbms.data.entities.values.RawValue;

public class DummyDPProvider implements DataPointsProvider {

	@Override
	public void init(Properties props, String name, String propertiesPrefix) {
	}

	@Override
	public void close() {
	}

	@Override
	public RawValue getDataPointValue(Address dataPoint, boolean allowCache) {
		return null;
	}

	@Override
	public Snapshot getDataPointValues(List<Address> dataPoints, boolean allowCache) {
		return null;
	}

	@Override
	public AggregatedValue getDataPointsAggregation(List<Address> dataPoints, AggregationFunction aggregation,
			boolean allowCache) {
		return null;
	}

	@Override
	public Map<String, AggregatedValue> getDataPointGroupAggregations(Map<String, List<Address>> dataPointGroups,
			AggregationFunction aggregation, boolean allowCache) {
		return null;
	}

	@Override
	public Map<String, Snapshot> getDataPointGroupedValues(Map<String, List<Address>> dataPoints, boolean allowCache) {
		return null;
	}

}
