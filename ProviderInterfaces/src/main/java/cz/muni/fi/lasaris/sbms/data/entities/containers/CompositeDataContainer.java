package cz.muni.fi.lasaris.sbms.data.entities.containers;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;

public abstract class CompositeDataContainer<L, D> {
	
	protected Map<L,D> container;
	
	public CompositeDataContainer() {
		container = new LinkedHashMap<L, D>();
	}
	
	public CompositeDataContainer(Map<L,D> data) {
		container = data; //new LinkedHashMap<L, D>();
		//container.putAll(data);
	}
	
	public void add(L label, D data) {
		container.put(label, data);
	}
	
	public void addAll(Map<L, D> data) {
		container.putAll(data);
	}
	
	public D remove(L label) {
		return container.remove(label);
	}
	
	public D get(L label) {
		return container.get(label);
	}
	
	public Set<L> getKeys() {
		return Collections.unmodifiableSet(container.keySet());
	}
	
	public Map<L, D> getData() {
		return Collections.unmodifiableMap(container);
	}
	
	public void forEach(BiConsumer<? super L,? super D> action) {
		container.forEach(action);
	}
	
}
