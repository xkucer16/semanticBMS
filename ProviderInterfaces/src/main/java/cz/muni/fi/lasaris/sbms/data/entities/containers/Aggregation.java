package cz.muni.fi.lasaris.sbms.data.entities.containers;

import java.util.Map;

import cz.muni.fi.lasaris.sbms.data.entities.values.AggregatedValue;

public class Aggregation<L> extends CompositeDataContainer<L, AggregatedValue> {

	public Aggregation() {
		super();
	}
		
	public Aggregation(Map<L, AggregatedValue> data) {
		super(data);
	}
}
