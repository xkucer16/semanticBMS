package cz.muni.fi.lasaris.sbms.data.entities;

public enum Interpolation {
	NONE,
	LAST_VALUE,
	NEXT_VALUE,
	LINEAR;
	
	public static Interpolation fromString(String s) {
		s = s.toUpperCase().trim().replace("_", "");
		if(s.equals("LASTVALUE")) {
			return Interpolation.LAST_VALUE;
		}
		if(s.equals("NEXTVALUE")) {
			return Interpolation.NEXT_VALUE;
		}
		if(s.equals("LINEAR")) {
			return Interpolation.LINEAR;
		}
		if(s.equals("NONE")) {
			return Interpolation.NONE;
		}
		
		throw new IllegalArgumentException("Unknown sampling type.");
	}
}
