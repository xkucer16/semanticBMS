package cz.muni.fi.lasaris.sbms.data.entities;

public enum AggregationFunction {
	NONE,
	AVG,
	TEMPORAL_AVG,
	WEIGHTED_AVG,
	WEIGHTED_TEMPORAL_AVG,
	MEDIAN,
	MIN,
	MAX,
	SUM,
	COUNT, 
	AND,
	OR, 
	RATIO,
	WEIGHTED_RATIO,
	TEMPORAL_RATIO,
	WEIGHTED_TEMPORAL_RATIO;
	
	public static AggregationFunction fromString(String agg) {
		switch(agg.toUpperCase().trim()) {
		case "COUNT":
			return AggregationFunction.COUNT;
		case "MAX":
			return AggregationFunction.MAX;
		case "MIN":
			return AggregationFunction.MIN;
		case "AVG":
			return AggregationFunction.AVG;
		case "MEDIAN":
			return AggregationFunction.MEDIAN;
		case "SUM":
			return AggregationFunction.SUM;
		case "TEMPORAL_AVG":
			return AggregationFunction.TEMPORAL_AVG;
		case "WEIGHTED_AVG":
			return AggregationFunction.WEIGHTED_AVG;
		case "WEIGHTED_TEMPORAL_AVG":
			return AggregationFunction.WEIGHTED_TEMPORAL_AVG;
		case "RATIO":
			return AggregationFunction.RATIO;
		case "TEMPORAL_RATIO":
			return AggregationFunction.TEMPORAL_RATIO;
		case "WEIGHTED_RATIO":
			return AggregationFunction.WEIGHTED_RATIO;
		case "WEIGHTED_TEMPORAL_RATIO":
			return AggregationFunction.WEIGHTED_TEMPORAL_RATIO;
		case "OR":
			return AggregationFunction.OR;
		case "AND":
			return AggregationFunction.AND;
		case "NONE":
			return AggregationFunction.NONE;
		default:
			throw new IllegalArgumentException("Unknown aggregation type.");
		}
		
	}
}
