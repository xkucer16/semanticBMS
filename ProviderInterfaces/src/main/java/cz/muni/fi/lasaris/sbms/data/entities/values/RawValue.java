package cz.muni.fi.lasaris.sbms.data.entities.values;

import cz.muni.fi.lasaris.sbms.data.entities.AggregationFunction;
import cz.muni.fi.lasaris.sbms.data.entities.DataType;
import cz.muni.fi.lasaris.sbms.data.entities.Interpolation;

public class RawValue extends Value {
	
	public RawValue(Object value, DataType dataType) {
		super(value, dataType);
		this.interpolation = Interpolation.NONE;
		this.aggregation = AggregationFunction.NONE;
	}
	
	public RawValue(long value) {
		super(value);
		this.interpolation = Interpolation.NONE;
		this.aggregation = AggregationFunction.NONE;
	}
	
	public RawValue(boolean value) {
		super(value);
		this.interpolation = Interpolation.NONE;
		this.aggregation = AggregationFunction.NONE;
	}
	
	public RawValue(double value) {
		super(value);
		this.interpolation = Interpolation.NONE;
		this.aggregation = AggregationFunction.NONE;
	}
	
	public RawValue(String value) {
		super(value);
		this.interpolation = Interpolation.NONE;
		this.aggregation = AggregationFunction.NONE;
	}
	
	private RawValue(boolean error, String msg) {
		super(true, msg);
		this.interpolation = Interpolation.NONE;
		this.aggregation = AggregationFunction.NONE;
	}
	
	public static RawValue getReadingError() {
		return new RawValue(true, null);
	}
	
	public static RawValue getReadingError(String msg) {
		return new RawValue(true, msg);
	}
}
