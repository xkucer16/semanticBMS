package cz.muni.fi.lasaris.sbms.data.entities;

public enum Sampling {
	NONE,
	DURATION,
	CRON;
	
	public static Sampling fromString(String s) {
		if(s.toUpperCase().trim().equals("DURATION")) {
			return Sampling.DURATION;
		}
		if(s.toUpperCase().trim().equals("CRON")) {
			return Sampling.CRON;
		}
		if(s.toUpperCase().trim().equals("NONE")) {
			return Sampling.NONE;
		}
		
		throw new IllegalArgumentException("Unknown sampling type.");
	}
}
