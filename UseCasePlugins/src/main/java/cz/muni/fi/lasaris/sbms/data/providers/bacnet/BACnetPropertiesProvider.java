package cz.muni.fi.lasaris.sbms.data.providers.bacnet;

import java.util.List;
import java.util.Map;
import java.util.Properties;

import cz.muni.fi.lasaris.sbms.data.entities.Address;

import cz.muni.fi.lasaris.sbms.data.entities.values.RawValue;
import cz.muni.fi.lasaris.sbms.data.providers.AbstractSimpleDataPointsProvider;


public class BACnetPropertiesProvider extends AbstractSimpleDataPointsProvider {

	@Override
	public void init(Properties props, String name, String propertiesPrefix) {
		if(!BACnetConnector.isInitialized()) {
			BACnetConnector.init(props, name, propertiesPrefix);
		}
	}

	@Override
	public void close() {
		if(BACnetConnector.isInitialized()) {
			BACnetConnector.getInstance().close();
		}

	}
	
	protected Map<Address, RawValue> getValues(List<Address> dataPoints, boolean allowCache) {
		return BACnetConnector.getInstance().getValues(dataPoints, allowCache);
	}

}
