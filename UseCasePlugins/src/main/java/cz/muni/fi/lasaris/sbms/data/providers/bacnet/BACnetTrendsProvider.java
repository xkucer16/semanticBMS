package cz.muni.fi.lasaris.sbms.data.providers.bacnet;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import cz.muni.fi.lasaris.sbms.data.entities.Address;
import cz.muni.fi.lasaris.sbms.data.entities.containers.Trend;
import cz.muni.fi.lasaris.sbms.data.providers.AbstractSimpleTrendsProvider;

public class BACnetTrendsProvider extends AbstractSimpleTrendsProvider {

	@Override
	public void init(Properties props, String name, String propertiesPrefix) {
		if(!BACnetConnector.isInitialized()) {
			// this should never happen - BAS connector should be initialized first
			BACnetConnector.init(props, name, propertiesPrefix);
		}

	}
	
	@Override
	public void close() {
		if(BACnetConnector.isInitialized()) {
			// this should never happen
			BACnetConnector.getInstance().close();
		}
	}

	@Override
	protected Map<Address, Trend> getRecords(List<Address> trends, ZonedDateTime from, ZonedDateTime to) {
		return BACnetConnector.getInstance().getRecords(trends, from, to);
	}

	@Override
	protected Map<Address, Trend> getClosestOlderRecord(List<Address> trends, ZonedDateTime timestamp) {
		return getClosestOlderRecord(trends, timestamp);
	}

	@Override
	protected Map<Address, Trend> getClosestNewerRecord(List<Address> trends, ZonedDateTime timestamp) {
		return getClosestNewerRecord(trends, timestamp);
	}
}
