package cz.muni.fi.lasaris.sbms.data.providers.bacnet;

import java.time.ZonedDateTime;

import cz.muni.fi.lasaris.sbms.data.entities.values.RawValue;

public class CachedValue  {
	
	private RawValue value;
	
	private ZonedDateTime timestamp;

	public ZonedDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(ZonedDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public RawValue getValue() {
		return value;
	}

	public void setValue(RawValue value) {
		this.value = value;
		this.timestamp = ZonedDateTime.now();
	}
	
	public void setValue(RawValue value, ZonedDateTime timestamp) {
		this.value = value;
		this.timestamp = timestamp;
	}
	
	public CachedValue(RawValue value) {
		this.value = value;
		this.timestamp = ZonedDateTime.now();
	}
	
	public boolean isValid(long validity) {
		return ZonedDateTime.now().isBefore(this.timestamp.plusSeconds(validity));
	}
}
