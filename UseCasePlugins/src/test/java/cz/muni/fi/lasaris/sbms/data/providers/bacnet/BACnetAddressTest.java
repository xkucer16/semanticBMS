package cz.muni.fi.lasaris.sbms.data.providers.bacnet;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import cz.muni.fi.lasaris.sbms.data.entities.Address;
import cz.muni.fi.lasaris.sbms.data.providers.bacnet.BACnetAddress;

public class BACnetAddressTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testBACnetAddressAddress() {
		BACnetAddress ba = null;
		
		ba = new BACnetAddress(new Address("bacnet://4600.AV.91"));
		assertArrayEquals(new int[] {4600, 2, 91, 85 }, new int[] {ba.getDevice(), ba.getType(), ba.getInstance(), ba.getProperty()});
		
		ba = new BACnetAddress(new Address("bacnet://4600.2.91"));
		assertArrayEquals(new int[] {4600, 2, 91, 85 }, new int[] {ba.getDevice(), ba.getType(), ba.getInstance(), ba.getProperty()});
		
		ba = new BACnetAddress(new Address("bacnet://4600.2.91.85"));
		assertArrayEquals(new int[] {4600, 2, 91, 85 }, new int[] {ba.getDevice(), ba.getType(), ba.getInstance(), ba.getProperty()});
		
		ba = new BACnetAddress(new Address("bacnet://4600.Analog_value.91.PRESENT_VALUE"));
		assertArrayEquals(new int[] {4600, 2, 91, 85 }, new int[] {ba.getDevice(), ba.getType(), ba.getInstance(), ba.getProperty()});
		
		ba = new BACnetAddress(new Address("4600.Analog_value.91.PRESENT_VALUE"));
		assertArrayEquals(new int[] {4600, 2, 91, 85 }, new int[] {ba.getDevice(), ba.getType(), ba.getInstance(), ba.getProperty()});
	}

}
