[CmdletBinding()]
Param([Int]$sites=5, [Int]$buildings=5, [Int]$floors=3, [Int]$rooms=10)
"dpId;dpType;source;scope;sensingType;sensingWindow;propDomain;propQuality;publisher;name"
for($i = 1; $i -le $sites; $i++) {
    for($j = 1; $j -le $buildings; $j++) {
        [System.String]::Format("bacnet://{0:00}{1:00}0101.AV1001;Input;EnergyMeter_S{0:00}B{1:00}F01R01;S{0:00}B{1:00};AggregateSumSensing;Eternity;Electricity;energy;PLC_S{0:00}B{1:00}F01R01;S{0:00}B{1:00}_Electricity",$i, $j, $k, $l)
        [System.String]::Format("bacnet://{0:00}{1:00}0101.AV1002;Input;EnergyMeter_S{0:00}B{1:00}F01R01;S{0:00}B{1:00};AggregateSumSensing;MonthsLast;Electricity;energy;PLC_S{0:00}B{1:00}F01R01;S{0:00}B{1:00}_ElectricityMonth",$i, $j, $k, $l)
        for($k = 1; $k -le $floors; $k++) {    
            for($l = 1; $l -le $rooms; $l++) {        
                #[System.String]::Format("Sensor_Temp_S{0:00}B{1:00}F{2:00}R{3:00};TemperatureSensor;S{0:00}B{1:00}F{2:00}R{3:00};TempSensor{0:00}{1:00}{2:00}{3:00}",$i, $j, $k, $l)
                [System.String]::Format("bacnet://{0:00}{1:00}{2:00}{3:00}.AI1;Input;Sensor_Temp_S{0:00}B{1:00}F{2:00}R{3:00};S{0:00}B{1:00}F{2:00}R{3:00};StatelessDirectSensing;;Air;temperature;PLC_S{0:00}B{1:00}F{2:00}R{3:00};S{0:00}B{1:00}F{2:00}R{3:00}_Temp",$i, $j, $k, $l)
                #[System.String]::Format("Sensor_RH_S{0:00}B{1:00}F{2:00}R{3:00};HumiditySensor;S{0:00}B{1:00}F{2:00}R{3:00};RHSensor{0:00}{1:00}{2:00}{3:00}",$i, $j, $k, $l)
                [System.String]::Format("bacnet://{0:00}{1:00}{2:00}{3:00}.AI2;Input;Sensor_RH_S{0:00}B{1:00}F{2:00}R{3:00};S{0:00}B{1:00}F{2:00}R{3:00};StatelessDirectSensing;;Air;fraction;PLC_S{0:00}B{1:00}F{2:00}R{3:00};S{0:00}B{1:00}F{2:00}R{3:00}_RH",$i, $j, $k, $l)
                #[System.String]::Format("Sensor_Movement_S{0:00}B{1:00}F{2:00}R{3:00};MovementSensor;S{0:00}B{1:00}F{2:00}R{3:00};MovementSensor{0:00}{1:00}{2:00}{3:00}",$i, $j, $k, $l)  
                [System.String]::Format("bacnet://{0:00}{1:00}{2:00}{3:00}.BI1;Input;Sensor_Movement_S{0:00}B{1:00}F{2:00}R{3:00};S{0:00}B{1:00}F{2:00}R{3:00};StatelessDirectSensing;;Software;level;PLC_S{0:00}B{1:00}F{2:00}R{3:00};S{0:00}B{1:00}F{2:00}R{3:00}_Occupancy",$i, $j, $k, $l)
                #[System.String]::Format("PLC_S{0:00}B{1:00}F{2:00}R{3:00};ProgrammableController;S{0:00}B{1:00}F{2:00}R{3:00};PLC{0:00}{1:00}{2:00}{3:00}",$i, $j, $k, $l)
                [System.String]::Format("bacnet://{0:00}{1:00}{2:00}{3:00}.AV1;UserDefined;PLC_S{0:00}B{1:00}F{2:00}R{3:00};S{0:00}B{1:00}F{2:00}R{3:00};StatelessDirectSensing;;Air;temperature;PLC_S{0:00}B{1:00}F{2:00}R{3:00};S{0:00}B{1:00}F{2:00}R{3:00}_SetPoint_Temp",$i, $j, $k, $l)
            }
        }
    }
}