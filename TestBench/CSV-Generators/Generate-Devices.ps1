[CmdletBinding()]
Param([Int]$sites=5, [Int]$buildings=5, [Int]$floors=3, [Int]$rooms=10)
"bimId;devType;room;name"
for($i = 1; $i -le $sites; $i++) {
    for($j = 1; $j -le $buildings; $j++) {
        [System.String]::Format("PLC_S{0:00}B{1:00}F01R01;ProgrammableController;S{0:00}B{1:00}F01R01;PLC{0:00}{1:00}0101",$i, $j, $k, $l)
        [System.String]::Format("EnergyMeter_S{0:00}B{1:00}F01R01;EnergyMeter;S{0:00}B{1:00}F01R01;EnergyMeter{0:00}{1:00}0101",$i, $j, $k, $l)
        for($k = 1; $k -le $floors; $k++) {    
            for($l = 1; $l -le $rooms; $l++) {        
                [System.String]::Format("Sensor_Temp_S{0:00}B{1:00}F{2:00}R{3:00};TemperatureSensor;S{0:00}B{1:00}F{2:00}R{3:00};TempSensor{0:00}{1:00}{2:00}{3:00}",$i, $j, $k, $l)
                [System.String]::Format("Sensor_RH_S{0:00}B{1:00}F{2:00}R{3:00};HumiditySensor;S{0:00}B{1:00}F{2:00}R{3:00};RHSensor{0:00}{1:00}{2:00}{3:00}",$i, $j, $k, $l)
                [System.String]::Format("Sensor_Movement_S{0:00}B{1:00}F{2:00}R{3:00};MovementSensor;S{0:00}B{1:00}F{2:00}R{3:00};MovementSensor{0:00}{1:00}{2:00}{3:00}",$i, $j, $k, $l)  
                [System.String]::Format("PLC_S{0:00}B{1:00}F{2:00}R{3:00};ProgrammableController;S{0:00}B{1:00}F{2:00}R{3:00};PLC{0:00}{1:00}{2:00}{3:00}",$i, $j, $k, $l)
            }
        }
    }
}