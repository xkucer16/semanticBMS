[CmdletBinding()]
Param([switch]$ManageTomcat,
[String]$TomcatHome = "\apache-tomcat",
[String]$User = "admin",
[String]$Pass = "nimda",
[String]$Server = "http://localhost:8080/sbms/semantics/",
[String]$TestsToRun = "1,2,3,4,5",
[Int]$Repeat = 1,
[switch]$PrintHeaders
)


Function Get-Auth($user, $pass) {
  $pair = "${user}:${pass}"
  $bytes = [System.Text.Encoding]::ASCII.GetBytes($pair)
  $base64 = [System.Convert]::ToBase64String($bytes)
  $basicAuthValue = "Basic $base64"
  return @{ Authorization = $basicAuthValue }
}

Function Request($testId, $testName, $auth, $url) {
  #"Sending..." + [System.DateTime]::Now
  $start = [System.DateTime]::Now
  $r = Invoke-WebRequest -Method Get -URI $url -Headers $auth
  $end = [System.DateTime]::Now.Subtract($start) #| Select-Object TotalSeconds
  $dur = $end.TotalSeconds
  $c = ConvertFrom-Json $r.Content
  $count = 0
  $cItems = 0
  if($c.groups -ne $null) {
    foreach($g in $c.groups) {
      $count += $c.results.$g.Count
    }
  }
  [System.String]::Format("{0:00.00}",$dur) + ";" + $testId + ";" + $testName + ";" + $r.StatusCode + ";" + $count
}

if($ManageTomcat) {
  If($env:JAVA_HOME -eq $null -or $env:JAVA_HOME -eq "") {
    "JAVA_HOME is not set - set the environment variable correctly to run Apache Tomcat"
    return;
  }
  $env:CATALINA_HOME=$TomcatHome
  & $TomcatHome\bin\startup.bat
  Pause
}

if($PrintHeaders) {
  "duration;id;testDesc;statusCode;dataPoints"
}
#initial request -- to init the tdb and mode
Request "0" "Init" $auth ($Server + "types/type")

$auth = Get-Auth $User $Pass

$tArray = $TestsToRun.Split(",")

for($i=0; $i -lt $Repeat; $i++){
if($tArray.Contains("UC")) {
  Request "UC1" "Trends - Average room temperatures" $auth ($Server + "trends/?fields=bmsId%2CdataPoint.bmsId&grouping=scope.building&dataPoint.type=Input&dataPoint.source.type=TemperatureSensor&dataPoint.source.location=S01&dataPoint.scope.type=Room&dataPoint.sensing.type=StatelessDirectSensing&dataPoint.property.domain=Air&dataPoint.property.quality=temperature")
  Request "UC2" "DP" $auth ($Server + "datapoints/?fields=bmsId,source.type&grouping=source.room&type=Input&source.type=Sensor&source.location=S01B04&scope.type=Room&property.domain=Air&sensing.type=StatelessDirectSensing")
}

if($tArray.Contains("1")) {
  Request "1"  "All information about a DP" $auth ($Server + "datapoints/?fields=bmsId%2Ctype%2Csource.bimId%2Csource.type%2Csource.location%2Cscope.bimId%2Cscope.type%2Cscope.location%2Csensing.type%2Csensing.window%2Cproperty.domain%2Cproperty.quality%2Cpublisher.bimId&bmsId=bacnet%3A%2F%2F04010306.AV1")
}

if($tArray.Contains("2")) {
  Request "2" "All DPs according to strict criteria + grouping" $auth ($Server + "datapoints/?fields=bmsId&grouping=scope.floor&type=Input&source.type=TemperatureSensor&source.location=S01B04&scope.type=Room&property.domain=Air&property.quality=temperature")
  Request "2" "The same query as above, different building" $auth ($Server + "datapoints/?fields=bmsId&grouping=scope.floor&type=Input&source.type=TemperatureSensor&source.location=S02B04&scope.type=Room&property.domain=Air&property.quality=temperature")
}

if($tArray.Contains("3")) {
  Request "3" "Generic query with large number of results + grouping" $auth ($Server + "datapoints/?fields=bmsId&grouping=scope.building&source.type=HumiditySensor&property.domain=Air")
}

if($tArray.Contains("4")) {
  Request "4" "Generic query with large number of results + datapoint type + grouping" $auth ($Server + "datapoints/?fields=bmsId%2Ctype%2Cscope.bimId&grouping=scope.building&source.type=TemperatureSensor&property.domain=Air&property.quality=temperature")
}

if($tArray.Contains("5")) {
  Request "5" "Generic query with large number of results + all available information + grouping" $auth ($Server + "datapoints/?fields=bmsId%2Ctype%2Csource.bimId%2Csource.type%2Csource.location%2Cscope.bimId%2Cscope.type%2Cscope.location%2Csensing.type%2Csensing.window%2Cproperty.domain%2Cproperty.quality%2Cpublisher.bimId&grouping=scope.building&source.type=TemperatureSensor&property.domain=Air&property.quality=temperature")
}
}
if($ManageTomcat) {
  Pause
  & $TomcatHome\bin\shutdown.bat
}