[CmdletBinding()]
Param(
[Int]$Sites = 5,
[Int]$Buildings = 5,
[Int]$Floors = 3,
[Int]$Rooms = 10,
[String]$OutFolder = ".\CSV-Data"
)

if(-not(Test-Path $OutFolder)) {
    mkdir $OutFolder
}

.\CSV-Generators\Generate-Rooms.ps1 -sites $Sites -buildings $Buildings -floors $Floors -rooms $Rooms | Out-File -Encoding utf8 $OutFolder\rooms.csv
.\CSV-Generators\Generate-Devices.ps1 -sites $Sites -buildings $Buildings -floors $Floors -rooms $Rooms | Out-File -Encoding utf8 $OutFolder\devices.csv
.\CSV-Generators\Generate-DataPoints.ps1 -sites $Sites -buildings $Buildings -floors $Floors -rooms $Rooms | Out-File -Encoding utf8 $OutFolder\datapoints.csv
.\CSV-Generators\Generate-Trends.ps1 -sites $Sites -buildings $Buildings -floors $Floors -rooms $Rooms | Out-File -Encoding utf8 $OutFolder\trends.csv


