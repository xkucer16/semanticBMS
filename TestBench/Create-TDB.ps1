[CmdletBinding()]
Param(
    [String]$TDBPath="\apache-tomcat\webapps\sbms\WEB-INF\tdb",
    [String]$NTPath=".\NT-Data\data.nt",
    [String]$JenaRoot="\apache-jena-3.2.0"
)

If(Test-Path $TDBPath) {
    Remove-Item $TDBPath -Recurse
}

$env:JENAROOT = $jenaRoot
$env:Path = $env:Path +";" + $jenaRoot + "\bin;" + $jenaRoot + "\bat"

& tdbloader.bat --loc=$TDBPath $NtPath
#tdbstats --loc=$TDBPath | Out-File TDB-Stats.txt
