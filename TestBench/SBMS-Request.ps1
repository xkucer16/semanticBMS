[CmdletBinding()]
Param([switch]$ManageTomcat,
[String]$TomcatHome = "\apache-tomcat",
[String]$User = "admin",
[String]$Pass = "nimda",
[String]$Server = "http://localhost:8080/sbms/semantics/",
[String]$Query = "types/type"
)

Function Get-Auth($user, $pass) {
  $pair = "${user}:${pass}"
  $bytes = [System.Text.Encoding]::ASCII.GetBytes($pair)
  $base64 = [System.Convert]::ToBase64String($bytes)
  $basicAuthValue = "Basic $base64"
  return @{ Authorization = $basicAuthValue }
}

$r = Invoke-WebRequest -Method Get -URI ($Server + $Query) -Headers (Get-Auth $User $Pass)

$r.StatusCode
$r.Content 

