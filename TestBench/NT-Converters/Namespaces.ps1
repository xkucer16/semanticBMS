$pref = @{ 
  rdf = "<http://www.w3.org/1999/02/22-rdf-syntax-ns#";
  owl = "<http://www.w3.org/2002/07/owl#";
  rdfs = "<http://www.w3.org/2000/01/rdf-schema#";
  xsd = "<http://www.w3.org/2001/XMLSchema#";
  ssn = "<http://purl.oclc.org/NET/ssnx/ssn#";
  sbim = "<http://is.muni.cz/www/255658/sbms/v2_0/SemanticBIM#";
  sbms = "<http://is.muni.cz/www/255658/sbms/v2_0/SemanticBMS#";
  sbmsd = "<http://is.muni.cz/www/255658/sbms/v2_0/SemanticBMSData#"
  ucum = "<http://purl.oclc.org/NET/muo/ucum/physical-quality/"
}