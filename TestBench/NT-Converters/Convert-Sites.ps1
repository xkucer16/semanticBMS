[CmdletBinding()]
Param([String]$Csv="rooms.csv")
. ((split-path -parent $MyInvocation.MyCommand.Definition) + "\Namespaces.ps1")
import-csv -Delimiter ";" $Csv | Select-Object site -Unique | ForEach-Object {
  ($pref["sbmsd"] + $_.site + "> " + $pref["rdf"] + "type" + "> " + $pref["sbim"] + "Site" + "> .")
  #($pref["sbmsd"] + $_.site + "> " + $pref["rdf"]+ "type" + "> " + $pref["owl"] + "NamedIndividual" + "> .")
  ($pref["sbmsd"] + $_.site + "> " + $pref["sbim"]+ "hasBIMId" + "> " + "`"" + $_.site + "`"" + " .")    
}

