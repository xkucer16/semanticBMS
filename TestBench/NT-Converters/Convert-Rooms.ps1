[CmdletBinding()]
Param([String]$Csv="rooms.csv")
. ((split-path -parent $MyInvocation.MyCommand.Definition) + "\Namespaces.ps1")

import-csv -Delimiter ";" $Csv | ForEach-Object {
  ($pref["sbmsd"] + $_.room + "> " + $pref["rdf"] + "type" + "> " + $pref["sbim"] + "Room" + "> .")
  #($pref["sbmsd"] + $_.room + "> " + $pref["rdf"]+ "type" + "> " + $pref["owl"] + "NamedIndividual" + "> .")
  ($pref["sbmsd"] + $_.room + "> " + $pref["sbim"]+ "hasBIMId" + "> " + "`"" + $_.room + "`"" + " .")
  ($pref["sbmsd"] + $_.room + "> " + $pref["sbim"] + "isRoomOf" + "> " + $pref["sbmsd"] + $_.floor + "> .")
  ($pref["sbmsd"] + $_.floor + "> " + $pref["sbim"] + "hasRoom" + "> " + $pref["sbmsd"] + $_.room + "> .")    
}
