[CmdletBinding()]
Param([String]$Csv="rooms.csv")
. ((split-path -parent $MyInvocation.MyCommand.Definition) + "\Namespaces.ps1")

import-csv -Delimiter ";" $Csv | Select-Object building, floor -Unique | ForEach-Object {
  ($pref["sbmsd"] + $_.floor + "> " + $pref["rdf"] + "type" + "> " + $pref["sbim"] + "Floor" + "> .")
  #($pref["sbmsd"] + $_.floor + "> " + $pref["rdf"]+ "type" + "> " + $pref["owl"] + "NamedIndividual" + "> .")
  ($pref["sbmsd"] + $_.floor + "> " + $pref["sbim"]+ "hasBIMId" + "> " + "`"" + $_.floor + "`"" + " .")
  ($pref["sbmsd"] + $_.floor + "> " + $pref["sbim"] + "isFloorOf" + "> " + $pref["sbmsd"] + $_.building + "> .")
  ($pref["sbmsd"] + $_.building + "> " + $pref["sbim"] + "hasFloor" + "> " + $pref["sbmsd"] + $_.floor + "> .")    
}
