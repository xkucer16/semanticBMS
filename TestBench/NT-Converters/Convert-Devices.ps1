[CmdletBinding()]
Param([String]$Csv="devices.csv")
. ((split-path -parent $MyInvocation.MyCommand.Definition) + "\Namespaces.ps1")

import-csv -Delimiter ";" $Csv | ForEach-Object {
  ($pref["sbmsd"] + $_.bimId + "> " + $pref["rdf"] + "type" + "> " + $pref["sbim"] + $_.devType + "> .")
  ($pref["sbmsd"] + $_.bimId + "> " + $pref["sbim"]+ "hasBIMId" + "> " + "`"" + $_.bimId + "`"" + " .")
  ($pref["sbmsd"] + $_.bimId + "> " + $pref["sbim"] + "hasInstallationInRoom" + "> " + $pref["sbmsd"] + $_.room + "> .")
  ($pref["sbmsd"] + $_.room + "> " + $pref["sbim"] + "isInstallationLocationOfDevice" + "> " + $pref["sbmsd"] + $_.bimId + "> .")
  ($pref["sbmsd"] + $_.bimId + "> " + $pref["rdfs"] + "label" + "> " + "`"" + [System.Security.SecurityElement]::Escape($_.name) + "`"" + " .")    
}
