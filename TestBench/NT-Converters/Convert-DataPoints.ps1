[CmdletBinding()]
Param([String]$Csv="datapoints.csv")
. ((split-path -parent $MyInvocation.MyCommand.Definition) + "\Namespaces.ps1")

import-csv -Delimiter ";" $Csv | ForEach-Object {
  $dpId=  "DataPoint" + $_.dpId.Replace("/","-").Replace(".","-").Replace(":","-")
  $obsId =  $dpId + "Obs"
  ($pref["sbmsd"] + $dpId + "> " + $pref["rdf"] + "type" + "> " + $pref["sbms"] + $_.dpType + "> .")
  ($pref["sbmsd"] + $obsId + "> " + $pref["rdf"] + "type" + "> " + $pref["sbms"] + "Observation> .")
  ($pref["sbmsd"] + $dpId + "> " + $pref["sbms"]+ "hasBMSId" + "> " + "`"" + $_.dpId + "`"" + " .")
  ($pref["sbmsd"] + $dpId + "> " + $pref["sbms"]+ "expressesObservation" + "> " + $pref["sbmsd"] + $obsId + "> .")
  ($pref["sbmsd"] + $dpId + "> " + $pref["sbms"]+ "publishedByDevice" + "> " + $pref["sbmsd"] + $_.publisher + "> .")
   $opId =  $_.scope + $_.propDomain + $_.propQuality + "ObsProp"
  ($pref["sbmsd"] + $opId + "> " + $pref["rdf"] + "type" + "> " + $pref["sbms"] + "ObservedProperty> .")
  ($pref["sbmsd"] + $opId + "> " + $pref["sbms"]+ "hasPhysicalQuality" + "> " + $pref["ucum"] + $_.propQuality + "> .")
  ($pref["sbmsd"] + $opId + "> " + $pref["sbms"]+ "hasPropertyDomain" + "> " + $pref["sbms"] + $_.propDomain + "> .")
  ($pref["sbmsd"] + $opId + "> " + $pref["sbms"]+ "isPropertyOf" + "> " + $pref["sbmsd"] + $_.scope + "> .")
  
  ($pref["sbmsd"] + $obsId + "> " + $pref["sbms"]+ "observedProperty" + "> " + $pref["sbmsd"] + $opId + "> .")
  ($pref["sbmsd"] + $obsId + "> " + $pref["sbms"]+ "observedBy" + "> " + $pref["sbmsd"] + $_.source + "> .")
  ($pref["sbmsd"] + $obsId + "> " + $pref["sbms"]+ "featureOfInterest" + "> " + $pref["sbmsd"] + $_.scope + "> .")
   $sensingId =  $dpId + "Sensing"
   ($pref["sbmsd"] + $sensingId + "> " + $pref["rdf"] + "type" + "> " + $pref["sbms"] + $_.sensingType + "> .")
   if($_.sensingWindow -ne "") {
    ($pref["sbmsd"] + $sensingId + "> " + $pref["sbms"]+ "hasAggregationTimeWindow" + "> " + $pref["sbmsd"] + $_.sensingWindow + "> .")
   }
  ($pref["sbmsd"] + $obsId + "> " + $pref["sbms"]+ "sensingMethodUsed" + "> " + $pref["sbmsd"] + $sensingId + "> .")
  
  ($pref["sbmsd"] + $dpId + "> " + $pref["rdfs"] + "label" + "> " + "`"" + [System.Security.SecurityElement]::Escape($_.name) + "`"" + " .")    
}
