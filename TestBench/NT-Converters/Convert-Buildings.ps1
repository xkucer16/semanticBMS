[CmdletBinding()]
Param([String]$Csv="rooms.csv")
. ((split-path -parent $MyInvocation.MyCommand.Definition) + "\Namespaces.ps1")

import-csv -Delimiter ";" $Csv | Select-Object site, building -Unique | ForEach-Object {
  ($pref["sbmsd"] + $_.building + "> " + $pref["rdf"] + "type" + "> " + $pref["sbim"] + "Building" + "> .")
  #($pref["sbmsd"] + $_.building + "> " + $pref["rdf"]+ "type" + "> " + $pref["owl"] + "NamedIndividual" + "> .")
  ($pref["sbmsd"] + $_.building + "> " + $pref["sbim"]+ "hasBIMId" + "> " + "`"" + $_.building + "`"" + " .")
  ($pref["sbmsd"] + $_.building + "> " + $pref["sbim"] + "isBuildingOf" + "> " + $pref["sbmsd"] + $_.site + "> .")
  ($pref["sbmsd"] + $_.site + "> " + $pref["sbim"] + "hasBuilding" + "> " + $pref["sbmsd"] + $_.building + "> .")    
}
