[CmdletBinding()]
Param([String]$Csv="trends.csv")
. ((split-path -parent $MyInvocation.MyCommand.Definition) + "\Namespaces.ps1")

import-csv -Delimiter ";" $Csv | ForEach-Object {
  $dpId = "DataPoint" + $_.dpId.Replace("/","-").Replace(".","-").Replace(":","-")
  $trendId= "Trend" + $_.trendId.Replace("/","-").Replace(".","-").Replace(":","-")
  ($pref["sbmsd"] + $trendId + "> " + $pref["rdf"] + "type" + "> " + $pref["sbms"] + "Trend> .")
  ($pref["sbmsd"] + $trendId + "> " + $pref["sbms"]+ "hasBMSId" + "> " + "`"" + $_.trendId + "`"" + " .")
  ($pref["sbmsd"] + $trendId + "> " + $pref["sbms"]+ "trends" + "> " + $pref["sbmsd"] + $dpId + "> .")   
}