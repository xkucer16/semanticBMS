 [CmdletBinding()]
Param(
    [String]$InFolder = ".\CSV-Data",
    [String]$OutFile = ".\NT-Data\data.nt"
)

.\Nt-Converters\Convert-Sites.ps1 -Csv $InFolder\rooms.csv | Out-File -Encoding utf8 $OutFile
.\Nt-Converters\Convert-Buildings.ps1 -Csv $InFolder\rooms.csv | Out-File -Append -Encoding utf8 $OutFile
.\Nt-Converters\Convert-Floors.ps1 -Csv $InFolder\rooms.csv | Out-File -Append -Encoding utf8 $OutFile
.\Nt-Converters\Convert-Rooms.ps1 -Csv $InFolder\rooms.csv | Out-File -Append -Encoding utf8 $OutFile
.\Nt-Converters\Convert-Devices.ps1 -Csv $InFolder\devices.csv | Out-File -Append -Encoding utf8 $OutFile
.\Nt-Converters\Convert-DataPoints.ps1 -Csv $InFolder\datapoints.csv | Out-File -Append -Encoding utf8 $OutFile
.\Nt-Converters\Convert-Trends.ps1 -Csv $InFolder\trends.csv  | Out-File -Append -Encoding utf8 $OutFile