package cz.muni.fi.lasaris.sbms.data.api.response;

import java.util.LinkedHashMap;
import java.util.Map;

import cz.muni.fi.lasaris.sbms.data.entities.Address;
import cz.muni.fi.lasaris.sbms.data.entities.containers.Aggregation;
import cz.muni.fi.lasaris.sbms.data.entities.values.AggregatedValue;

public class SetOfAggregatesResponse {
	private static final String DEFAULT_GROUPING = "noGrouping";

	private String error;
	private Map<String, Map<Address, AggregatedValue>> results;

	public Map<String, Map<Address, AggregatedValue>> getResults() {
		return this.results;
	}

	public static SetOfAggregatesResponse getErrorResponse(String error) {
		SetOfAggregatesResponse r = new SetOfAggregatesResponse();
		r.setError(error);
		return r;
	}

	private SetOfAggregatesResponse() {
		this.results = null;
	}
	
	public SetOfAggregatesResponse(Map<String, Map<Address, AggregatedValue>> data) {
		this.results = data;
	}
	/*
	public SetOfAggregatesResponse(Map<String, Aggregation<Address>> data) {
		this.results = new LinkedHashMap<String, Map<Address, AggregatedValue>>();
		for(Entry<String, Aggregation<Address>> e : data.entrySet()) {
			Map<Address, AggregatedValue> a = new LinkedHashMap<Address, AggregatedValue>();
			e.getValue().forEach((k,v) -> a.put(k, v));
			this.results.put(e.getKey(), a);
		}
	}
	*/
	public SetOfAggregatesResponse(Address a, AggregatedValue v) {
		this.results = new LinkedHashMap<String, Map<Address, AggregatedValue>>();

		this.results.put(DEFAULT_GROUPING, new LinkedHashMap<Address, AggregatedValue>());
		this.results.get(DEFAULT_GROUPING).put(a, v);
	}

	public SetOfAggregatesResponse(Aggregation<Address> a) {
		this.results = new LinkedHashMap<String, Map<Address, AggregatedValue>>();
		this.results.put(DEFAULT_GROUPING, new LinkedHashMap<Address, AggregatedValue>());
		a.forEach((k,v) -> this.results.get(DEFAULT_GROUPING).put(k, v));
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
