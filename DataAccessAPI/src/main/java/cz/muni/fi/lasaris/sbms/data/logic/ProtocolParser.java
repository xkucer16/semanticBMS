package cz.muni.fi.lasaris.sbms.data.logic;

import java.util.LinkedList;
import java.util.List;
import java.util.NavigableSet;

import cz.muni.fi.lasaris.sbms.data.api.request.AddressRequest;
import cz.muni.fi.lasaris.sbms.data.api.request.GroupedAddressRequest;
import cz.muni.fi.lasaris.sbms.data.api.request.TrendRequest;
import cz.muni.fi.lasaris.sbms.data.entities.Address;

public class ProtocolParser {
	private AddressRequest readSpecs;
	private GroupedAddressRequest greadSpecs;
	
	public ProtocolParser(AddressRequest readSpecs) {
		this.readSpecs = readSpecs;
	}
	
	public ProtocolParser(GroupedAddressRequest readSpecs) {
		this.greadSpecs = readSpecs;
	}
	
	public ProtocolParser(TrendRequest readSpecs) {
		if(readSpecs.isGropuing()) {
			this.greadSpecs = readSpecs.getGroupedAddressSpecs();
		} else {
			this.readSpecs = readSpecs.getAddressSpecs();
		}
	}
	
	public NavigableSet<String> getProtocolsSet() {
		NavigableSet<String> protocols = null;
		if(readSpecs != null) {
			protocols = readSpecs.getProtocols().navigableKeySet();
		}
		if(greadSpecs != null) {
			protocols = greadSpecs.getProtocols().navigableKeySet();
		}
		return protocols;
	}
	
	public List<Address> getAddresses(String p) {
		List<Address> a = null;
		if(readSpecs != null) {
			a = readSpecs.getProtocols().get(p).getAddresses();
		}
		if(greadSpecs != null) {
			a = new LinkedList<Address>();
			for(List<Address> g : greadSpecs.getProtocols().get(p).getGroups().values()) {
				a.addAll(g);
			}
		}
		return a;
	}
}
