package cz.muni.fi.lasaris.sbms.data.api.response;


import cz.muni.fi.lasaris.sbms.data.entities.values.Value;

public class ValueResponse {
	
	private String error;
	private Value result;
	
	public Value getResult() {
		return this.result;
	}
	
	public static ValueResponse getErrorResponse(String error) {
		ValueResponse r = new ValueResponse();
		r.setError(error);
		return r;
	}
	
	private ValueResponse() {
		this.result = null;
	}
	
	public ValueResponse(Value data) {
		this.result = data;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
