package cz.muni.fi.lasaris.sbms.data.api.response;

import java.util.LinkedHashMap;
import java.util.Map;

import cz.muni.fi.lasaris.sbms.data.entities.Address;
import cz.muni.fi.lasaris.sbms.data.entities.containers.Snapshot;
import cz.muni.fi.lasaris.sbms.data.entities.values.RawValue;

public class SnapshotResponse {
	
	private static final String DEFAULT_GROUPING = "noGrouping";
	
	private String error;
	private Map<String, Snapshot> results;
	
	public Map<String, Snapshot> getResults() {
		return this.results;
	}
	
	public static SnapshotResponse getErrorResponse(String error) {
		SnapshotResponse r = new SnapshotResponse();
		r.setError(error);
		return r;
	}
	
	private SnapshotResponse() {
		this.results = null;
	}
	
	public SnapshotResponse(Map<String, Snapshot> data) {
		this.results = data;
	}
	
	public SnapshotResponse(Snapshot data) {
		this.results = new LinkedHashMap<String, Snapshot>();
		this.results.put(DEFAULT_GROUPING, data);
	}
	
	public SnapshotResponse(Address a, RawValue v) {
		this(new Snapshot());
		this.results.get(DEFAULT_GROUPING).add(a, v);
		
	}
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
