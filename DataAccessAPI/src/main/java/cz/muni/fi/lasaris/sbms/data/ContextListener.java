package cz.muni.fi.lasaris.sbms.data;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

@WebListener
public class ContextListener implements ServletContextListener {
	final static Logger logger = Logger.getLogger(ContextListener.class);
	
	public ContextListener() {
		logger.debug("Custom ContextListener initialized.");
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent c) {
		logger.debug("Closing the SBMS Data API...");
		Application.close();
		logger.debug("Closed.");
	}

	@Override
	public void contextInitialized(ServletContextEvent c) {
	}

}
