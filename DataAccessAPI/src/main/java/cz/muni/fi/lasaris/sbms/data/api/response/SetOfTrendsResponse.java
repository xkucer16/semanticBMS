package cz.muni.fi.lasaris.sbms.data.api.response;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import cz.muni.fi.lasaris.sbms.data.entities.Address;
import cz.muni.fi.lasaris.sbms.data.entities.containers.Trend;
import cz.muni.fi.lasaris.sbms.data.entities.containers.TrendGroup;

public class SetOfTrendsResponse {
	private static final String DEFAULT_GROUPING = "noGrouping";
	
	private String error;
	private Map<String, Map<Address, Trend>> results;
	
	public Map<String, Map<Address, Trend>> getResults() {
		return this.results;
	}
	
	public static SetOfTrendsResponse getErrorResponse(String error) {
		SetOfTrendsResponse r = new SetOfTrendsResponse();
		r.setError(error);
		return r;
	}
	
	private SetOfTrendsResponse() {
		this.results = null;
	}
	
	/*
	public SetOfTrendsResponse(Map<String, Map<Address, Trend>> data, boolean groups) {
		this.results = data;
	}
	*/
	
	
	public SetOfTrendsResponse(Map<String, TrendGroup> data, boolean groups) {
		this.results = new LinkedHashMap<String, Map<Address, Trend>>();
		for(Entry<String, TrendGroup> e : data.entrySet()) {
			Map<Address, Trend> a = new LinkedHashMap<Address, Trend>();
			e.getValue().forEach((k,v) -> a.put(k, v));
			this.results.put(e.getKey(), a);
		}
	}
	
	public SetOfTrendsResponse(Map<Address, Trend> data) {
		this.results = new LinkedHashMap<String, Map<Address, Trend>>();
		this.results.put(DEFAULT_GROUPING, data);
	}
	
	public SetOfTrendsResponse(Address a, Trend v) {
		this.results = new LinkedHashMap<String, Map<Address, Trend>>();
		this.results.put(DEFAULT_GROUPING, new LinkedHashMap<Address, Trend>());
		this.results.get(DEFAULT_GROUPING).put(a, v);
	}
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
