package cz.muni.fi.lasaris.sbms.data.api.request;

import java.util.Arrays;

import cz.muni.fi.lasaris.sbms.data.entities.Address;
import cz.muni.fi.lasaris.sbms.data.entities.AggregationFunction;
import cz.muni.fi.lasaris.sbms.data.entities.SeriesSpecs;

public class TrendRequest {

	private AddressRequest ar;
	private GroupedAddressRequest gar;
	private SeriesSpecs s;
	private AggregationFunction af;

	public Address getAddress() {
		return ar.getAddresses().get(0);
	}

	public void setSeriesSpecs(SeriesSpecs s) {
		this.s = s;
	}

	public void setAggregation(AggregationFunction aggregation) {
		this.af = aggregation;
	}
	
	public void setAddressSpecs(Address a) {
		AddressRequest ar = new AddressRequest();
		ar.setAddresses(Arrays.asList(new Address[] { a }));
		setAddressSpecs(ar);
	}
	
	public void setAddressSpecs(AddressRequest ar) {
		this.ar = ar;
		
	}
	
	public void setAddressSpecs(GroupedAddressRequest gar) {
		this.gar = gar;
	}
	
	public AddressRequest getAddressSpecs() {
		return this.ar;
	}
	
	public GroupedAddressRequest getGroupedAddressSpecs() {
		return this.gar;
	}
	
	public SeriesSpecs getSeriesSpecs() {
		return this.s;
	}
	
	public AggregationFunction getAggregation() {
		return this.af;
	}
	
	public boolean isGropuing() {
		return this.gar != null;
	}
}
