package cz.muni.fi.lasaris.sbms.data.api.response;

import java.util.LinkedHashMap;
import java.util.Map;

import cz.muni.fi.lasaris.sbms.data.entities.Address;
import cz.muni.fi.lasaris.sbms.data.entities.containers.Slice;
import cz.muni.fi.lasaris.sbms.data.entities.values.InterpolatedValue;

public class SliceResponse {
	
	private static final String DEFAULT_GROUPING = "noGrouping";
	
	private String error;
	private Map<String, Slice> results;
	
	public Map<String, Slice> getResults() {
		return this.results;
	}
	
	public static SliceResponse getErrorResponse(String error) {
		SliceResponse r = new SliceResponse();
		r.setError(error);
		return r;
	}
	
	private SliceResponse() {
		this.results = null;
	}
	
	public SliceResponse(Map<String, Slice> data) {
		this.results = data;
	}
	
	public SliceResponse(Slice data) {
		this.results = new LinkedHashMap<String, Slice>();
		this.results.put(DEFAULT_GROUPING, data);
	}
	
	public SliceResponse(Address a, InterpolatedValue v) {
		this(new Slice());
		this.results.get(DEFAULT_GROUPING).add(a, v);
		
	}
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
