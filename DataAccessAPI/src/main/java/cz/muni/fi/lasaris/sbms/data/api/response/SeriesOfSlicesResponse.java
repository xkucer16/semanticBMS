package cz.muni.fi.lasaris.sbms.data.api.response;

import java.util.LinkedHashMap;
import java.util.Map;

import cz.muni.fi.lasaris.sbms.data.entities.containers.Series;
import cz.muni.fi.lasaris.sbms.data.entities.containers.Slice;

public class SeriesOfSlicesResponse {
private static final String DEFAULT_GROUPING = "noGrouping";
	
	private String error;
	private Map<String, Series<Slice>> results;
	
	public Map<String, Series<Slice>> getResults() {
		return this.results;
	}
	
	public static SeriesOfSlicesResponse getErrorResponse(String error) {
		SeriesOfSlicesResponse r = new SeriesOfSlicesResponse();
		r.setError(error);
		return r;
	}
	
	private SeriesOfSlicesResponse() {
		this.results = null;
	}
	
	public SeriesOfSlicesResponse(Map<String, Series<Slice>> data) {
		this.results = data;
	}
	
	
	public SeriesOfSlicesResponse(Series<Slice> data) {
		this.results = new LinkedHashMap<String, Series<Slice>>();
		this.results.put(DEFAULT_GROUPING, data);
	}
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
