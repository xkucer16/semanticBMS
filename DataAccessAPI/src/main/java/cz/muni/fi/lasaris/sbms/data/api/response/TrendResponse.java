package cz.muni.fi.lasaris.sbms.data.api.response;

import java.util.LinkedHashMap;
import java.util.Map;

import cz.muni.fi.lasaris.sbms.data.entities.containers.Trend;

public class TrendResponse {
	
	private static final String DEFAULT_GROUPING = "noGrouping";
	
	private String error;
	private Map<String, Trend> results;
	
	public Map<String, Trend> getResults() {
		return this.results;
	}
	
	public static TrendResponse getErrorResponse(String error) {
		TrendResponse r = new TrendResponse();
		r.setError(error);
		return r;
	}
	
	private TrendResponse() {
		this.results = null;
	}
	
	public TrendResponse(Map<String, Trend> data) {
		this.results = data;
	}
	
	
	public TrendResponse(Trend data) {
		this.results = new LinkedHashMap<String, Trend>();
		this.results.put(DEFAULT_GROUPING, data);
	}
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
}
