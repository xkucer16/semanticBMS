package cz.muni.fi.lasaris.sbms.data.api.response;

import java.util.LinkedHashMap;
import java.util.Map;

import cz.muni.fi.lasaris.sbms.data.entities.AddressGroup;
import cz.muni.fi.lasaris.sbms.data.entities.containers.Aggregation;
import cz.muni.fi.lasaris.sbms.data.entities.values.AggregatedValue;

public class AggregateResponse {
	private static final String DEFAULT_GROUPING = "noGrouping";
	
	private String error;
	private Map<String, AggregatedValue> results;
	
	public Map<String, AggregatedValue> getResults() {
		return this.results;
	}
	
	public static AggregateResponse getErrorResponse(String error) {
		AggregateResponse r = new AggregateResponse();
		r.setError(error);
		return r;
	}
	
	private AggregateResponse() {
		this.results = null;
	}
	
	public AggregateResponse(Map<String, AggregatedValue> data) {
		this.results = data;
	}
	
	public AggregateResponse(AggregatedValue v) {
		this.results = new LinkedHashMap<String, AggregatedValue>();
		this.results.put(DEFAULT_GROUPING, v);
	}
	
	public AggregateResponse(Aggregation<AddressGroup> a) {
		this.results = new LinkedHashMap<String, AggregatedValue>();
		a.forEach((k,v) -> this.results.put(k.getName(), v));
	}
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
