package cz.muni.fi.lasaris.sbms.data.test;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.NavigableMap;

import org.junit.Before;
import org.junit.Test;

import cz.muni.fi.lasaris.sbms.data.api.request.GroupedAddressRequest;
import cz.muni.fi.lasaris.sbms.data.api.request.AddressRequest;
import cz.muni.fi.lasaris.sbms.data.entities.Address;

public class RequestTests {
	
	private Address[] a;
	
	@Before
	public void setUp() throws Exception {
		a = new Address[] {
				new Address("bacnet://4600.AV91.85"),
				new Address("lon://1.44.895"),
				new Address("bacnet://4600.AV92.85"),
				new Address("bacnet://4601.AV91.85"),
				new Address("historian://93.TL1845"),
				new Address("lon://1.48.895")
		};
		
	}

	@Test
	public void testGroupedSnapshotRequestGetProtocols() {
		GroupedAddressRequest gr = new GroupedAddressRequest();
		gr.setGroups(new LinkedHashMap<String, List<Address>>());
		List<Address> l1 = new LinkedList<Address>();
		List<Address> l2 = new LinkedList<Address>();
		l1.add(a[0]);
		l1.add(a[1]);
		l1.add(a[2]);
		l2.add(a[3]);
		l2.add(a[4]);
		l2.add(a[5]);
		gr.getGroups().put("BHA10", l1);
		gr.getGroups().put("BHA11", l2);
		
		NavigableMap<String, GroupedAddressRequest> t = gr.getProtocols();
		assertEquals(3, t.size());
		
		assertEquals(2 ,t.get("lon").getGroups().size());
		assertEquals(Arrays.asList(a[1]), t.get("lon").getGroups().get("BHA10"));
		assertEquals(Arrays.asList(a[5]), t.get("lon").getGroups().get("BHA11"));
		
		assertEquals(2 ,t.get("bacnet").getGroups().size());
		assertEquals(Arrays.asList(a[0], a[2]), t.get("bacnet").getGroups().get("BHA10"));
		assertEquals(Arrays.asList(a[3]), t.get("bacnet").getGroups().get("BHA11"));
		
		assertEquals(1 ,t.get("historian").getGroups().size());
		assertEquals(Arrays.asList(a[4]), t.get("historian").getGroups().get("BHA11"));
	}
	
	@Test
	public void testSnapshotRequestGetProtocols() {
		AddressRequest gr = new AddressRequest();
		List<Address> l = new LinkedList<Address>();
		l.add(a[0]);
		l.add(a[1]);
		l.add(a[2]);
		l.add(a[3]);
		l.add(a[4]);
		l.add(a[5]);
		gr.setAddresses(l);
		
		NavigableMap<String, AddressRequest> t = gr.getProtocols();
		assertEquals(3, t.size());
		
		assertEquals(2 ,t.get("lon").getAddresses().size());
		assertEquals(Arrays.asList(a[1], a[5]), t.get("lon").getAddresses());
		
		
		assertEquals(3 ,t.get("bacnet").getAddresses().size());
		assertEquals(Arrays.asList(a[0], a[2], a[3]), t.get("bacnet").getAddresses());
		
		
		assertEquals(1 ,t.get("historian").getAddresses().size());
		assertEquals(Arrays.asList(a[4]), t.get("historian").getAddresses());
	}
}
